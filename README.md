# tegaki_save

某所で使用する手書き保存ツールです。

[ダウンロードはこちら](https://gitlab.com/HARUKei66494739/tegaki_save/-/releases "リリース")

## ディレクトリ構成
tegaki_saveディレクトリ
tegaki_save本体のディレクトリです。基本はこちらの中身をchrome拡張としてご使用ください。

tegaki_save/maked_pluginsディレクトリ
プラグインを個別に格納するためのディレクトリですが、プラグインについては検討中です。
基本的に触らなくて大丈夫です。

## 使い方

使い方については以下のページを参照してください。

https://docs.google.com/spreadsheets/d/1lTMp8uxetq4DBACaWkYbg8-se_atLt2cmHlgDRGqVQs/edit?usp=sharing