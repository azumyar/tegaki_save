// read.check.ng.public
// 読み上げ時のプラグイン[NG機能 共通辞書]
// Ver2.1.0

// プラグイン用実行関数（コメント読み上げの前処理）
export const pluginList = [
  {
    point: "read",
    sort: "010",
    beforeExecute: function (param) {
      return checkNg(param);;
    }
  }
];

// NG機能 共通辞書処理
function checkNg(param) {
  // 処理結果返り値
  // isStop：処理の中断判定…返した側で処理を中断すべきかどうかの判定値（true：処理を中断させる、false：処理続行）
  // isError：エラーの有無 （true：エラー発生、エラーの種類によっては処理を続行しない 返した側で判断する）
  // message：中断やエラーのメッセージ
  // resultValue：呼び出し元に返したい値
  let result = {isStop : false, isError : false, message : "", resultValue : ""};
  
  // メイン処理
  let list = new Array();
  // 共通NGワードの配列(matchTypeが0で正規表現なし、1で正規表現あり)
  list[list.length] = {matchType : "1" , value : "\&lt;(.*?)\&gt;"};  // 本文内にタグを含む場合
  list[list.length] = {matchType : "1" , value : "((匿名(掲示板|コミュニティ)|自己(主張|顕示欲満々)|雑魚配信者|小遣い稼ぎ|スタートアップ|宣伝|乞食|コジキ|(お外|おそと|他所|よそ|ヨソ)で(やって|やれ|お願い)|にお帰りください|お客(さま|様)|巣に帰れ|ゴミ配信)[\s\S]*){2}|コテと(信者|取り巻き)|(キモい|気持ち悪い|くっさい)馴れ合い|ぶいちゅーばー|ついった～|「」のフリしろ|馴れ合う場所"};           // NGにしたいワードを""内に入れる(正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};
  list[list.length] = {matchType : "0" , value : ""};

  // コメントから改行タグを除去
  let comment = param.res.com.replace(/<br>/g, '');
  if(comment == "") return result; // 空の場合、処理を行わない

  // 共通NGワードを含むかどうか判定
  for (let i = 0; i < list.length; i++) {
    if(list[i].value == "") continue; // 空の場合、スキップする

    // 正規表現なしでチェック
    if(list[i].matchType == "0" && comment.indexOf(list[i].value) != -1) {
      console.log("NG機能 共通NG辞書 beforeExecute read.check.ng.public", (i+1).toString() + " 番目のNG設定にヒット", "対象コメント:" + comment);
      // ヒットした場合、処理を継続させない
      result.isStop = true;
      break;

    } else if(list[i].matchType == "1" && comment.search(new RegExp(list[i].value)) != -1) {
      console.log("NG機能 共通NG辞書 beforeExecute read.check.ng.public", (i+1).toString() + " 番目のNG設定にヒット", "対象コメント:" + comment);
      // ヒットした場合、処理を継続させない
      result.isStop = true;
      break;
    }

  }

  return result;
}
