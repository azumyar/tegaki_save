// read.check.ng.user
// 読み上げ時のプラグイン[NG機能 ユーザ辞書]
// Ver2.1.0

// プラグイン用実行関数（コメント読み上げの前処理）
export const pluginList = [
  {
    point: "read",
    sort: "020",
    beforeExecute: function (param) {
      return checkNg(param);
    }
  }
];

// プラグイン用実行関数（コメント読み上げの前処理）
function checkNg(param) {
  // 処理結果返り値
  // isStop：処理の中断判定…返した側で処理を中断すべきかどうかの判定値（true：処理を中断させる、false：処理続行）
  // isError：エラーの有無 （true：エラー発生、エラーの種類によっては処理を続行しない 返した側で判断する）
  // message：中断やエラーのメッセージ
  // resultValue：呼び出し元に返したい値
  let result = {isStop : false, isError : false, message : "", resultValue : ""};
  
  // ============================================================================================================
  // 編集箇所（以下の範囲内をユーザNG辞書として編集してください）
  // ============================================================================================================
  // ユーザ登録のNGワードの配列(matchTypeが0で正規表現なし、1で正規表現あり)
  let list = new Array();
  list[list.length] = {matchType : "0" , value : "におスレ"};    // 本文内に含む場合(サンプルとして追加してあるワード)
  list[list.length] = {matchType : "1" , value : "^(?=.*シャンカー)(?=.*チェンソーマン).*"};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "1" , value : "^https?://img.2chan.net/b/res/[0-9]+.htm$"};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "1" , value : "^ｷﾀ━+\\(ﾟ∀ﾟ\\)━+\\s!+[^!]"};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)
  list[list.length] = {matchType : "0" , value : ""};           // NGにしたいワードを""内に入れる(matchTypeを1にすることで正規表現可能)

  let resComment = new Array();
  resComment[resComment.length] = "";  // NGにヒットした時に読み上げたいワード
  resComment[resComment.length] = "";  // NGにヒットした時に読み上げたいワード
  resComment[resComment.length] = "";  // NGにヒットした時に読み上げたいワード
  resComment[resComment.length] = "";  // NGにヒットした時に読み上げたいワード
  resComment[resComment.length] = "";  // NGにヒットした時に読み上げたいワード

  // ============================================================================================================

  // コメントから改行タグを除去
  let comment = param.res.com.replace(/<br>/g, '');
  if(comment == "") return result; // 空の場合、処理を行わない

  // NGワードを含むかどうか判定
  for (let i = 0; i < list.length; i++) {
    if(list[i].value == "") continue; // 空の場合、スキップする

    // 正規表現なしでチェック
    if(list[i].matchType == "0" && comment.indexOf(list[i].value) != -1) {
      console.log("NG機能 ユーザNG辞書 beforeExecute read.check.ng.user", (i+1).toString() + " 番目のNG設定にヒット", "対象コメント:" + comment);
      // ヒットした場合、処理を継続させない
      result.isStop = true;
      break;

    } else if(list[i].matchType == "1" && comment.search(new RegExp(list[i].value)) != -1) {
      console.log("NG機能 ユーザNG辞書 beforeExecute read.check.ng.user", (i+1).toString() + " 番目のNG設定にヒット", "対象コメント:" + comment);
      // ヒットした場合、処理を継続させない
      result.isStop = true;
      break;
    }
  }
  // ヒットしている場合
  if(result.isStop) {
    if(resComment.join("") != "") {
      let speakText = [];
      for(let i = 0;i < resComment.length;i++) {
        if(resComment[i] == "") continue; // 空の場合、スキップする
        speakText.push(resComment[i]);
      }

      if(speakText.length > 0){
        bgSendMessage({case: "speak", text: speakText, port: tegakiStorage.portNumber});
      }
    }
  }

  return result;
}
