// tegaki_plugin_manager.js
// プラグイン管理
// Ver2.1.0

//プラグイン情報
let tegakiPlugins = {
  load : [],
  save : [],
  read : [],
  start : [],
  status404 : []
}; 

// プラグインJSファイルのロード
function pluginsJsLoad(plugins) {
  // pluginsフォルダ内になるプラグインのロード
  plugins.forEach(function(file) {
    // プラグインフォルダ内のplugin.jsでexportされた内容をimportで取得
    import(`../plugins/${file}/plugin.js`).then(function(modules) {
      // プラグインのポイント別にtegakiPlugins内に格納（実行並び順はあとで並び変える）
      for(let i = 0;i < modules.pluginList.length;i++) {
        // plugin.js内にある{point,sort,beforeExecute,afterExecute,execute}のような形式の値を該当pointのプラグイン内に追加する
        tegakiPlugins[modules.pluginList[i].point].push(modules.pluginList[i]);
      }
      for(let prop in tegakiPlugins){
        tegakiPlugins[prop].sort((a, b) => a.sort.localeCompare(b.sort));
      }
    });
  });
}

// 読み込んだプラグインをまとめて実行
function runPlugins(param) {
  // 処理結果返り値
  // isStop：処理の中断判定…返した側で処理を中断すべきかどうかの判定値（true：処理を中断させる、false：処理続行）
  // isError：エラーの有無 （true：エラー発生、エラーの種類によっては処理を続行しない 返した側で判断する）
  // message：中断やエラーのメッセージ
  // resultValue：呼び出し元に返したい値
  let r = {isStop : false, isError : false, message : "", resultValue : ""};
  let pr = {};
  // 上記で決めたオブジェクト名や処理名をもとに処理を実行
  // (各JSファイル内にある処理を実行する)
  try {
    for(let i = 0;i < tegakiPlugins[param.point].length;i++) {
      // 該当関数がある場合のみ実行
      // （pnameがread、execNameがbeforeExecuteの場合、「tegakiPlugins.read[i].module.beforeExecute」を実行）
      if(tegakiPlugins[param.point][i][param.execName]) {
        pr = tegakiPlugins[param.point][i][param.execName](param);
        r.resultValue = pr.resultValue;
        // 中断の場合、中断して処理を抜ける
        if(pr.isStop) {
          r.isStop = true;
          r.message = tegakiPlugins[param.point][i].name + "で処理中断";
          break;
        }
      }
    }
  } catch(e){
    r.isError = true;
    r.message = tegakiPlugins[param.point][i].name + "でエラー 内容：" + e.name + ":" + e.message;
  }
  return r;
}
