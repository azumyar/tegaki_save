// script.js
// Ver2.1.0

// ダイアログのクラス格納用
let tegakiDialog = null;

// スレッドライブ機能の情報
let thrLive = {
  autoScroll: true,          // 自動スクロールフラグ
  tegakiSave: true           // 手書き保存の実施中フラグ
};

// 手書き保存制御情報
let tegaki = {
  version : "",                         // 現バージョン（manifest.json空取得して設定）
  tabId: "",                            // この画面のタブID
  param: {},                            // この画面の呼び出し時に渡された値
  wavAudio: {},                         // wavファイル
  isStarted: false,                     // 手書き保存開始中かどうか
  isThreadLive: true,                   // スレッドが生きてるかどうか
  isThreadOld: false,                   // もうすぐスレッドが落ちるかどうか
  resList: [],                          // 取得済みレス情報リスト
  storedResNo: "",                      // 読み上げ済みレスNo(resIndexは廃止しました)
  resNo: "",                            // 読み上げ済み最大レスNo(php要求用)
  tegakiIndex: 0,                       // 保存済み手書きindex
  shioIndex: 0,                         // 保存済み塩辛瓶(あぷ)index
  threadDielong: "",                    // 公式json内にあるスレ落ち時刻（GMT）\directory\game\Left Alive
  serverTime: 0,                        // 公式json内にあるサーバー時間
  lastReadTextTime: {},                 // 前回レスの取得時間（ウィンドウが隠れているなどのオンクルージョンアラート対応用）(time：時間、isAlerted：通知済み)
  lastActiveCheckTime: {},              // 前回アクティブチェック時間（ウィンドウが隠れているなどのオンクルージョンアラート対応用）(time：時間、isAlerted：通知済み)
  processList: ["なし", "なし", "なし", "なし", "なし"],  // 実行プロセスリスト（最大5）
  initResIndex: null,                      // 途中から始めるときのレスindex
  alertBouyomi: false,                  // 棒読みちゃんアラート表示
  alertFileDownload: false,             // ファイルダウンロードエラー
  alertBackground: false,               // バックグランドエラー

  setting: {},                          // storageのsetting格納先
  sIndex: 0,                            // 塩辛用のIndex
  tIndex: 0,                            // 手書き用のIndex
  delLock: false,                       // IDレスdel中のロック(多分もう使ってない)
  notClear: false,                      // 透過pngで上書きしたかどうかのフラグ
  nextTime: 10000,                      // 検索間隔

  threadName: window.location.href.split('/').pop().split('.')[0],  // スレNO.
  threadTitle: document.title,          // スレッドタイトル
  threadHonbun: '',                     // スレッド本文
  threadImgUrl: ''                      // スレッド画像URL
};

// tegakiStorageオブジェクト型内のグローバル変数
let tegakiStorage = {
  // 設定値
  autoRun: false,       // 本文に以下の文字がある場合自動スタート
  keyword: "",          // 条件
  autoLive: false,      // ふたクロの自動更新も開始
  overwrite: false,     // 透過pngで上書きする
  saveSpan: 10000,      // 保存間隔(ミリ秒)
  fixWidth: false,      // 横幅を固定する
  widthPx: 344,         // 横幅(px)
  otherSave: false,     // 元ファイルを残す
  otherDir: "$Thread",  // 保存場所
  customSave: false,    // デフォサイズ以外を別フォルダに保存する
  customDir: "",        // 保存場所
  shioSave: false,      // 塩辛瓶のファイルを保存する
  shioDir: "",          // 保存場所
  threadNo: false,      // スレッドNoのtxtを保存する
  changeNo: false,      // スレが落ちたらtxtを変える
  changeNoTxt: "",      // 変更後txt

  startType: 0,         // 開始時に読み上げ
  startText: "手書き保存を開始します", // 読み上げ文章
  saveType: 0,          // 保存時に読み上げ
  saveText: "手書きを保存しました",  // 読み上げ文章
  alarmType: 0,         // 5分前にアラームを鳴らす
  alarmText: "もうすぐスレッドが落ちます", // 読み上げ文章
  alarmTime: 5,         // 実行時間

  endAlarm: false,      // スレ立てアラーム
  endAlarmText: "もうすぐスレッドが落ちます", // 読み上げ文章
  endAlarmTime: 5,      // 実行時間
  endAlarmChk: false,   // スレ立てアラーム

  portNumber: 50080,    // ポート番号(デフォルトは50080)
  autoDel: false,       // IDのレスを自動で削除する
  readRes: false,       // レス読み上げを行う
  resTag: "",           // レス文章に付ける特殊タグ
  readIDRes: false,     // IDのレスを読まない
  nonDialog: false,     // ダイアログ画面を表示しない
  plugins: ["all.execute.first","read.check.ng.public","read.check.ng.user"]    // プラグイン名の配列、初期値設定
};

// DOMロード時のイベント
window.addEventListener("load", function () {
  // バージョン番号設定
  const manifest = chrome.runtime.getManifest();
  tegaki.version = manifest.version;

  chrome.storage.local.get("setting", function (value) {
    const setting = value?.setting || tegakiStorage;
    const pluginsInit = tegakiStorage.plugins;
    const plugins = setting.plugins || pluginsInit; // storageの値がない場合、初期値をセット
    // プラグインのロード
    pluginsJsLoad(plugins);
    // storageのプラグインリストが空だった場合、初期値を保存
    if(!setting.plugins) {
      setting.plugins = pluginsInit;
      chrome.storage.local.set({ 'setting': setting }, function () {});
    }
  });
});

// コンテンツスクリプトが稼動するタブ内のイベントリスナ
chrome.runtime.onMessage.addListener(function (message, _, sendResponse) {

  let t = new TegakiSaveCtrl();
  // タブIDを設定
  if (message.case === 'updateTab') {
    tegaki.tabId = message.tabId;
    // 初期表示処理を実行
    tabsUpdatedLoad();
  } else if (tegaki.isStarted && message?.case === 'download' && message.error) {
    // ファイルダウンロード時のアラート(何度も表示され内容にフラグで判定)
    // 読み上げエラー時のアラート(何度も表示され内容にフラグで判定)
    if (!tegaki.alertFileDownload) {
      tegaki.alertFileDownload = true;
      // 画面にメッセージを表示する
      document.getElementById("dispStatus3").innerHTML = `<strong>networkエラーか何かでファイルを保存できませんでした ファイル名：${message.filename} </strong>`;
      // 読み上げで知らせる
      const talkParams = {
        type: 2,
        file: "",
        text: `手書き保存ツールです networkエラーか何かでファイルを保存できませんでした このお知らせは１度だけ行われます`,
      };
      t.talk(talkParams);
    }

  } else if (tegaki.isStarted && message?.case === 'speak' && message.error) {
    // 読み上げエラー時のアラート(何度も表示され内容にフラグで判定)
    if (!tegaki.alertBouyomi) {
      tegaki.alertBouyomi = true;
      // レス取得を停止してボタンをグレイアウトにする
      thrLive.tegakiSave = false;
      document.getElementById("liveButton").style.backgroundColor = "gray";
      // 画面にメッセージを表示する
      document.getElementById("dispStatus3").innerHTML = '<strong>棒読みちゃんが見つかりません</strong>';

      setTimeout(() => alert("手書き保存ツールです\n棒読みちゃんが見つかりません\n念のため手書き保存を中断します"), 500);
    }
  } else if (tegaki.isStarted && message?.case === 'timeout') {
    // 手書き保存を開始している かつ バックグラウンドのタイムアウト実行処理からの要求受付
    const funcList = {
      readText: readText,
      mainLoop: mainLoop,
      alarmLoop: alarmLoop,
      sendEndAlarm: sendEndAlarm,
      IDLoop: IDLoop
    };

    const funcName = message.name;
    const execFunc = funcList[funcName];
    if (execFunc) {
      execFunc();
    }
  }
  sendResponse({ error: false, case: message.case, text: message.text });
  return true;
});

///////////////////
// 設定値読み込み
// ページがロードされたら実行する
function tabsUpdatedLoad() {
  // 既に手書き保存開始済みの場合は以下の処理を実行しない
  // （ふたクロの書き込みフォーム用対応）
  if(tegaki.isStarted) {
    return;
  }
  chrome.storage.local.get("setting", function (value) {
    tegaki.setting = value.setting;

    if (tegaki.setting) {
      tegakiStorage.autoRun = tegaki.setting.autoRun;   // 本文に以下の文字がある場合自動スタート
      tegakiStorage.keyword = tegaki.setting.keyword;   // 条件
      tegakiStorage.autoLive = tegaki.setting.autoLive;  // ふたクロの自動更新も開始

      tegakiStorage.overwrite = tegaki.setting.overwrite; // 透過pngで上書きする
      tegakiStorage.saveSpan = tegaki.setting.saveSpan * 1000;  // 保存間隔(ミリ秒)
      tegakiStorage.fixWidth = tegaki.setting.fixWidth;  // 横幅を固定する
      tegakiStorage.widthPx = tegaki.setting.widthPx;   // 横幅(px)

      tegakiStorage.otherSave = tegaki.setting.otherSave; // 元ファイルを残す
      if (tegakiStorage.otherSave && tegaki.setting.otherDir) {
        tegakiStorage.otherDir = tegaki.setting.otherDir;  // 保存場所
      }

      tegakiStorage.customSave = tegaki.setting.customSave;// デフォサイズ以外を別フォルダに保存する
      if (tegakiStorage.customSave && tegaki.setting.otherDir) {
        tegakiStorage.customDir = tegaki.setting.customDir;  // 保存場所
      }

      tegakiStorage.shioSave = tegaki.setting.shioSave;  // 塩辛瓶のファイルを保存する
      if (tegakiStorage.shioSave && tegaki.setting.shioDir) {
        tegakiStorage.shioDir = tegaki.setting.shioDir;  // 保存場所
      }

      tegakiStorage.threadNo = tegaki.setting.threadNo;  // スレッドNoのtxtを保存する
      tegakiStorage.changeNo = tegaki.setting.changeNo;  // スレが落ちたらtxtを変える
      tegakiStorage.changeNoTxt = tegaki.setting.changeNoTxt;  // 変更後txt
      tegakiStorage.startType = tegaki.setting.startType; // 開始時に読み上げ
      tegakiStorage.startText = tegaki.setting.startText; // 読み上げ文章
      tegakiStorage.saveType = tegaki.setting.saveType;  // 保存時に読み上げ
      tegakiStorage.saveText = tegaki.setting.saveText;  // 読み上げ文章
      tegakiStorage.alarmType = tegaki.setting.alarmType;  // アラーム方式(wavファイル再生 / 棒読みちゃん連携)
      tegakiStorage.alarmTime = tegaki.setting.alarmTime; // 実行時間
      tegakiStorage.alarmText = tegaki.setting.alarmText;  // 読み上げ文章
      tegakiStorage.portNumber = tegaki.setting.portNumber;// ポート番号(デフォルトは50080)
      tegakiStorage.autoDel = tegaki.setting.autoDel;   // IDのレスを自動で削除する
      tegakiStorage.readRes = tegaki.setting.readRes;   // レス読み上げを行う
      tegakiStorage.resTag = tegaki.setting.resTag;    // レス文章に付ける特殊タグ
      tegakiStorage.readIDRes = tegaki.setting.readIDRes; // IDのレスを読まない

      tegakiStorage.endAlarm = tegaki.setting.endAlarm;     // スレ立てアラーム
      tegakiStorage.endAlarmText = tegaki.setting.endAlarmText; // 読み上げ文章
      tegakiStorage.endAlarmTime = tegaki.setting.endAlarmTime; // 実行時間

      tegakiStorage.nonDialog = tegaki.setting.nonDialog;  // ダイアログ画面を表示しない
      tegakiStorage.plugins = tegaki.setting.plugins;      // プラグイン名の配列（拡張機能のフォルダ内を取得できるのがpopup内のみであるためstorageを利用）
    } else {
      alert("手書き保存ツールです\n必要な初期値が設定されていません\n拡張機能のアイコンをクリックして設定画面を一度表示させてください")
    }
    // オーディオファイルをロードしておく
    tegaki.wavAudio["start.wav"] = new Audio(chrome.runtime.getURL('../wav/start.wav'));
    tegaki.wavAudio["save.wav"] = new Audio(chrome.runtime.getURL('../wav/save.wav'));
    tegaki.wavAudio["alarm.wav"] = new Audio(chrome.runtime.getURL('../wav/alarm.wav'));
    tegaki.wavAudio["activealert.wav"] = new Audio(chrome.runtime.getURL('../wav/activealert.wav'));

    // スレッド本文情報などを取得
    const honbun = document.querySelector('.thre');

    // 本文とURLを取得
    tegaki.threadHonbun = honbun.querySelector('blockquote').innerHTML;
    tegaki.threadImgUrl = honbun.querySelector('img').getAttribute("src");

    // ダイアログ画面を生成（ここではまだ非表示で生成のみ）
    tegakiDialog = new TegakiDialog();


    // id表示でスレ立てしてる場合、id表示でのスレ立てなのでIDレス自動削除は停止、ID読み飛ばしも停止
    if (honbun.querySelector('.cnw').innerHTML.includes('id表示')) {
      tegakiStorage.autoDel = false;
      tegakiStorage.readIDRes = false;
    }

    // ---------------------------
    // ロード時のプラグインを実行
    // ---------------------------
    let pResult = runPlugins({ point: "load", execName: "execute", from: "tabsUpdatedLoad" });
    if (pResult.isStop || pResult.isError) {
      console.error(pResult.message);
    }

    // 自動開始
    if (tegakiStorage.autoRun) {
      if (tegakiStorage.keyword && tegaki.threadHonbun.includes(tegakiStorage.keyword)) {
        // 開始ボタンクリック
        // 注）WAVファイル再生にはユーザーがドキュメントとの対話(クリック、キー入力など)が必要なため
        // 　　自動開始とWAVファイル再生をする場合、クリック・キー入力するまで再生されない
        setTimeout(() => document.getElementById("tegakiButton").click(), 1000);
      }
    }
    
  });
};

// 手書自動保存開始ボタン押下処理
function saveStart(paramMode) {
  // 手書き保存開始状態にする
  tegaki.isStarted = true;

  // タブのタイトルを新スレ状態にする
  document.title = "[Live開始!]" + tegaki.threadTitle;
  setTimeout(() => document.title = "[Live中...]" + tegaki.threadTitle, 60000); // １分後タイトルを変更

  // ダイアログ画面を表示しないがfalseの時にダイアログを表示
  if(!tegakiStorage.nonDialog) {
    // 手書き保存画面を表示
    tegakiDialog.show();
  }

  // 途中からの場合
  if (paramMode == "middle") {
    tegaki.initResIndex = document.getElementsByClassName("rsc").length;
  }

  // IDのレスを自動で削除するの場合
  if (tegakiStorage.autoDel) {
    IDLoop();
  }

  try {
    // スレ立てチェック
    chrome.storage.local.get("setting", function (value) {
      let setting = value.setting;
      if (setting) {
        setting.endAlarmChk = false;  // スレ立て忘れフラグ
        chrome.storage.local.set({ 'setting': setting }, function () { });
      }
    });
  } catch(e) {
    console.log('スレ立てチェックの初期化でエラーが発生したが、スレ立てチェックのみに影響するため処理を続行する');
  }

  if (Number.parseInt(tegakiStorage.alarmType,10) !== 0 || tegakiStorage.endAlarm) {
    alarmLoop();  // アラーム処理
  }

  let t = new TegakiSaveCtrl();
  // 手書き保存開始の通知
  if (Number.parseInt(tegakiStorage.startType,10) !== 0) {
    const talkParams = {
      type: tegakiStorage.startType,
      file: "start.wav",
      text: tegakiStorage.startText,
    };

    t.talk(talkParams);
  }

  // スレッドNo.txtの保存
  if (tegakiStorage.threadNo) {
    const threadParams = {
      file: `${tegaki.threadName}.htm`
    };
    t.thread(threadParams);
  }

  // ---------------------------
  // スタート時のプラグインを実行
  // ---------------------------
  let pResult = runPlugins({ point: "start", execName: "execute", from: "saveStart" });
  if (pResult.isStop || pResult.isError) {
    console.error(pResult.message);
  }

  // 読み上げ開始（レス情報の取得を行う必要があるため、readResがfalseの場合も実行する）
  readText();
  bgSendMessage({ case: "timeout", name: "mainLoop", time: 5000 });
  // 念のため開放
  t = null;
  // ふたクロの自動更新も開始
  if (tegakiStorage.autoLive) {
    var liveButton = document.getElementById("autolive");
    if (liveButton) {
      if (liveButton?.className === "f_menu_dot_right") {
        liveButton.click();
      }
    }
  }
}

// 手書きファイル保存、あぷファイル保存（10秒ごと）を行う関数
function mainLoop() {
  // スレッドが落ちている場合、処理を終わる
  if (!tegaki.isThreadLive) {
    return;
  }
  // 塩・あぷ・あぷ小ファイル保存
  if (tegakiStorage.shioSave) {
    saveShioFile();
  }
  // 手書きファイル保存
  const nextTime = saveTegakiFile();
  bgSendMessage({ case: "timeout", name: "mainLoop", time: nextTime });
}

// レスの取得と読み上げを行う
function readText() {
  // console.log(`readText:${new Date()}`);

  // ライブ中でない場合、読み上げを行わない
  if (!thrLive.tegakiSave) {
    bgSendMessage({ case: "timeout", name: "readText", time: 5000 });
    return;
  }

  let t = new TegakiSaveCtrl();
  // スレッドが落ちていない場合、読み上げを行う
  if (tegaki.isThreadLive) {
    // 手書き制御にread処理要求
    t.read();
    // read処理後の次の５秒後にレスを取得（棒読みへの要求はバックグラウンドのため非同期）
    bgSendMessage({ case: "timeout", name: "readText", time: 5000 });
  }

  // 念のため開放
  t = null;
}

// 手書きファイル保存
function saveTegakiFile() {
  tegakiDialog.processMessage("手書きファイル確認処理の実行中…");

  let foundFlg = false;
  let t = new TegakiSaveCtrl();
  let nextTime = tegaki.nextTime;

  // 既に取得済みの手書き保存情報.取得済みレス情報リストを基に処理を行う
  // 参照渡しをさせないためにスプレッド構文(中身を全部展開する)をつかって格納する
  let resList = [...tegaki.resList];
  for (let i = tegaki.tegakiIndex; i < resList.length; i++) {
    // 削除されたレスは読み飛ばし
    // IDのレスを読まないの時、IDが空じゃない場合 読み飛ばし（ID表示スレの考慮はしない）
    if (resList[i].del || (tegakiStorage.readIDRes && resList[i].id)) {
      tegaki.tegakiIndex = i + 1;
      continue;
    }
    // 手書きファイルのレスの場合
    if ((resList[i].fsize > 0) && (resList[i].ext != null) && (resList[i].ext != "")) {
      foundFlg = true;
      const message = {
        file: "tegaki.png",
        url: `https://img.2chan.net${resList[i].src}`,
        fixWidth: tegakiStorage.fixWidth,
        widthPx: tegakiStorage.widthPx,
        custom: false,
        res: { ...resList[i] } // レス情報をスプレッド構文で渡す
      };
      // 手書き制御にsave処理要求
      t.save(message);

      if (tegakiStorage.otherSave) {
        // tegakiStorage.otherDirが空だとエラーになるので対策
        const otherFile = tegakiStorage.otherDir && (0 < tegakiStorage.otherDir.length)
          ? `${tegakiStorage.otherDir.replace("$Thread", tegaki.threadName)}/${resList[i].tim}${resList[i].ext}` : `${resList[i].tim}${resList[i].ext}`;
        const otherMessage = {
          file: otherFile,
          url: `https://img.2chan.net${resList[i].src}`,
          fixWidth: false,
          custom: false,
          res: { ...resList[i] } // レス情報をスプレッド構文で渡す
        };
        if (tegakiStorage.customSave) {
          otherMessage.custom = true;
          otherMessage.customName = tegakiStorage.customDir && (0 < tegakiStorage.customDir.length)
            ? `${tegakiStorage.customDir.replace("$Thread", tegaki.threadName)}/${resList[i].tim}${resList[i].ext}` : `${resList[i].tim}${resList[i].ext}`;
        }
        // 手書き制御にsave処理要求
        t.save(otherMessage);
      }

      if (Number.parseInt(tegakiStorage.saveType,10) !== 0) {
        // 手書き制御にtalk処理要求
        tegakiDialog.processMessage("手書き保存の通知実行中…");
        const talkParams = {
          type: tegakiStorage.saveType,
          file: "save.wav",
          text: tegakiStorage.saveText,
        };
        t.talk(talkParams);

      }
      // スレッドを読み直すとtegaki.pngの上書きが間に合わないので手書きを１つ保存したらbreakする
      // 保存済み手書き最終レスindexを設定
      tegaki.tegakiIndex = i + 1;
      break;

    }
  }
  if (foundFlg) {
    tegaki.notClear = true;
    // 透過pngで上書きする場合、表示時間(秒)を設定する
    nextTime = tegakiStorage.overwrite ? tegakiStorage.saveSpan : nextTime;
  } else if (tegaki.notClear && tegakiStorage.overwrite) {
    // 未透過状態 かつ 透過pngで上書きするが設定されている場合、透過処理を実行する
    t.clear();
    tegaki.notClear = false;
  }  

  // 念のため開放
  t = null;
  return nextTime;
}

// 塩・あぷ・あぷ小ファイル保存
function saveShioFile() {
  tegakiDialog.processMessage("あぷファイル確認処理の実行中…");

  let t = new TegakiSaveCtrl();

  // 既に取得済みの手書き保存情報.取得済みレス情報リストを基に処理を行う
  // 参照渡しをさせないためにスプレッド構文(中身を全部展開する)をつかって格納する
  const resList = [...tegaki.resList];

  breakouter:
  for (let i = tegaki.shioIndex; i < resList.length; i++) {
    // 削除されたレスは読み飛ばし
    // IDのレスを読まないの時、IDが空じゃない場合 読み飛ばし（ID表示スレの考慮はしない）
    if (resList[i].del || (tegakiStorage.readIDRes && resList[i].id)) {
      tegaki.shioIndex = i + 1;
      continue;
    }

    const result = resList[i].com.split("<br>");
    for (const textWithTags of result) {
      // タグを除去
      // 絵文字がHTML Entity形式（&amp;#000000;)の形式になっているので文字に復元する
      const text = textWithTags.replace(/<("[^"]*"|'[^']*'|[^'">])*>/g, '')
        .replace(/&#(.*?);/g, (m, p1) => String.fromCodePoint(`0${p1}`));
      // 該当レスを判定
      if (!/([^>]|^|\s)(fu|f)[0-9]+\.(jpg|jpeg|png|gif|bmp|webm|mp4)/.test(text)) {
        continue;
      }
      if (/^&gt;/.test(text)) {
        continue;
      }
      const match = text.match(/(fu|f)[0-9]+\.(jpg|jpeg|png|gif|bmp|webm|mp4)/);

      const bottle = match[1];
      const shioValue = match[0];
      let url;
      switch (bottle) {
        case "fu":
          url = `https://dec.2chan.net/up2/src/${shioValue}`;
          break;
        case "f":
          url = `https://dec.2chan.net/up/src/${shioValue}`;
          break;
      }

      // tegakiStorage.shioDirが空だとエラーになるので対策
      const shioFile = tegakiStorage.shioDir && (0 < tegakiStorage.shioDir.length)
        ? `${tegakiStorage.shioDir.replace("$Thread", tegaki.threadName)}/${shioValue}` : shioValue;
      const message = {
        file: shioFile,
        url: url,
        fixWidth: false,
        custom: false,
        res: { ...resList[i] }
      };
      // 手書き制御にsave処理要求
      t.save(message);
      // 塩（あぷ）ファイル保存済み最終レスindexを設定
      tegaki.shioIndex = i + 1;
      // スレッドを読み直すと上書きが間に合わないので手書きを１つ保存したらbreakする
      break breakouter;

    }
  }

  // 念のため開放
  t = null;
}

// スレ立てアラーム
function sendEndAlarm() {
  tegakiDialog.processMessage("スレ立てアラームチェックの実行中…");

  let t = new TegakiSaveCtrl();

  chrome.storage.local.get("setting", function (value) {
    let setting = value.setting;
    if (setting) {
      tegakiStorage.endAlarmChk = setting.endAlarmChk;  // スレ立てアラーム
      if (tegakiStorage.endAlarmChk) {
        const talkParams = {
          type: 2,
          file: "end.wav",
          text: tegakiStorage.endAlarmText,
        };

        // 手書き制御にtalk処理要求
        tegakiDialog.processMessage("スレッドが立ってない の通知実行中…");
        t.talk(talkParams);
        // 念のため開放
        t = null;
      }
    }
  });
}

///////////////////
// IDレス削除処理
function IDLoop() {
  // スレッドが落ちている場合、処理を終わる
  if (!tegaki.isThreadLive) {
    return;
  }
  // 取得済みのレス情報を元に処理をする
  for (const res of [...tegaki.resList]) {
    // 削除されていないidありのレスのみ削除する
    if(!res.del && res.id) {
      const elements = document.querySelectorAll("#tegakiThread .tecno");
      for (let i = 0; i < elements.length; i++) {
        if (elements[i].innerText === `No.${res.resNo}`) {
          // 連続delにするとエラーになりそうなので１つ削除したらbreakする（手動の時は２秒くらい待たされたような）
          tegakiUsrDelSend(resList[i].resNo, elements[i].parentNode)
          break;
        }
      }
    }
  }

  bgSendMessage({ case: "timeout", name: "IDLoop", time: 10000 });
}

///////////////////
// アラーム処理
function alarmLoop() {
  tegakiDialog.processMessage("アラーム処理の実行中…");

  // スレ落ち時刻が空の場合、すぐ終了
  if (tegaki.threadDielong == "") {
    bgSendMessage({ case: "timeout", name: "alarmLoop", time: 10000 });
    return;
  }

  let t = new TegakiSaveCtrl();

  var dieLong = new Date(tegaki.threadDielong);

  // スレ落ちまでの残り分数を算出
  var lestTime = dieLong.getTime() - tegaki.serverTime;
  var lestMinute = parseInt(lestTime / (60 * 1000), 10);

  // 設定した残分数前未満の場合、アラーム
  if (Number(tegakiStorage.alarmTime) > Number(lestMinute)) {
    if (Number.parseInt(tegakiStorage.alarmType,10) !== 0) {
      // もうすぐスレッドが落ちます
      const talkParams = {
        type: tegakiStorage.alarmType,
        file: "alarm.wav",
        text: tegakiStorage.alarmText,
      };
      // 手書き制御にtalk処理要求
      tegakiDialog.processMessage("もうすぐスレッドが落ちますの通知実行中…");
      t.talk(talkParams);
    }
    if (tegakiStorage.endAlarm) {
      // スレ立てチェック
      chrome.storage.local.get("setting", function (value) {
        let setting = value.setting;
        if (setting) {
          setting.endAlarmChk = true;  // スレ立て忘れフラグ
          chrome.storage.local.set({ 'setting': setting }, function () { });
        }
      });
    }
  } else {
    bgSendMessage({ case: "timeout", name: "alarmLoop", time: 10000 });
  }
  // 念のため開放
  t = null;
}

// バックグラウンドへ処理要求
function bgSendMessage(message) {
  try{
    // バックグラウンドへの処理要求
    chrome.runtime.sendMessage(message, function (response) {
      // レスポンスは受け取るが、非同期のような処理が多いのでここでレスポンスを受け取っても意味がない。
      // 明確に処理後のタイミングで実行するため、backgroundからのsendMessageを当ファイル内のchrome.runtime.onMessage.addListenerで結果を受け取る
      if(chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
      }
    });
  } catch(e){
    console.info("bgSendMessage error:", e); 
    if(!tegaki.alertBackground) {
      tegaki.alertBackground = true;
      alert("手書き保存ツールです\nバックグランド処理が死んでるっぽいのでお手数ですがページを再読み込みしてもう一度開始してください\n今は上手くいってるっぽく表示されますが異常な状態なのでもう一度開始してください");
    }
  }
}

// 手書き保存処理制御クラス
// (値参照時の競合防止のためにオブジェクト化したらなんかクラス化した方が良いらしいのでクラス化した)
class TegakiSaveCtrl {
  // コンストラクタ
  constructor() {
  }

  // 読み上げ開始などの通知
  talk(param) {
    // wavファイル再生の場合
    if (param.type == 1) {
      try {
        // 指定ファイルを再生
        tegaki.wavAudio[param.file].play();
      } catch (e) {
        alert(`手書き保存ツールです\n指定されたファイルのwavファイルが見つかりません\n(再生しようとしたwavファイル[---${param.file}---])`);
      }
    } else {
      // バックグラウンドへ棒読みちゃん読み上げ処理要求
      bgSendMessage({ case: "speak", text: [param.text], port: tegakiStorage.portNumber });
    }
  }

  // 「threadNo.txt」の保存
  thread(param) {
    tegakiDialog.processMessage("「threadNo.txt」の保存実行中…");
    const fileContent = param.file; // 任意の文字列
    const fileName = 'threadNo.txt';
    const fileBlob = new Blob([fileContent], { type: 'text/plain;charset=utf-8' });
    const downloadOptions = {
      url: URL.createObjectURL(fileBlob),
      filename: fileName,
      conflictAction: 'overwrite'
    };

    // バックグラウンドへファイルダウンロード処理要求
    bgSendMessage({ case: "download", downloadOptions: downloadOptions });

  }

  // 読み上げ
  read() {
    tegakiDialog.processMessage("新着レスの監視実行中…");
    
    const speakText = [];
    const xmlHttpApi = new XMLHttpRequest();    
    try {
      // 上記以外の場合、レスを取得して読み上げ
      // スレッド内の全レスを公式phpへの要求でえられるJSON形式で取得する（データとして扱いやすい）
      xmlHttpApi.open('GET', `https://img.2chan.net/b/futaba.php?mode=json&res=${tegaki.threadName}&start=${tegaki.resNo}&${Math.random()}`, false);
      xmlHttpApi.send(null);

      // 公式phpへの要求でえられる情報の中身 2023/11/17現在
      // 名言なき場合値はnullにならない
      /*
        {
          die: string 画面表示用の消滅時間
          dielong: string スレ消滅予想時間 例: Sat, 20 May 2023 18:32:41 GMT
          dispname: int 0の場合この板では名前題名を表示しない
          dispsod: int 不明
          maxres: string 最大レス数に達した場合メッセージが入る
          nowtime: int サーバの現在時刻(UNIX時間、秒) 
          old: int スレが古い場合1
          res: { // 取得指示から先新着レスの連想配列、新着レスがない場合undefined
            (string:レスNo) : {
              com: string レスHTML
              del: string 削除された場合値が入るdel, del2, selfdel
              email: string 投稿時に入力されたメール 
              ext: string レス画像がある場合拡張子が入る
              fsize: int レス画像がある場合ファイルサイズが入る(特定条件下で値が入っているが再現方法不明)
              h: int レス画像サムネがの高さ
              host: string 赤字の場合ホスト情報が格納される、管理人が制御他目欄に（・ε・）といれると入る
              id: string idがでると入る id表示スレの場合は入らないがdelでid状態になるとidが入る
              name: string 投稿時に入力された名前
              now: string 投稿時間、画面表示用
              rsc: int レスのソート順番、ただしスレ落ち後に先行するレスが消滅した場合rscもデクリメントされるのでスレ内でユニークの保障はない
              src: string レス画像 レス画像がない場合空文字
              sub: string 投稿時に入力された題名
              thumb: string: サムネのURL、レス画像がない場合空文字
              tim: string 投稿UNIIX時間(ミリ秒) 添付レス画像ファイル名と一致する
              w: int レス画像サムネの横幅
            }
          }
          sd: [] // そうだねがスレないに存在しない場合sdは空の配列
          sd: { // そうだねが存在する場合sdは連想配列
            (string: レスNo) : int そうだねの数
          }
        }
      */
      const resJson = JSON.parse(xmlHttpApi.responseText);

      // 手書き保存情報にスレ落ち時間を設定
      // 後でスレ落ちアラームに利用
      tegaki.threadDielong = resJson.dielong;
      tegaki.serverTime = resJson.nowtime * 1000; // ミリ秒にする
      tegaki.isThreadOld = resJson.old == "1" ? true : false;

      // 新着レスがない場合resはundefined
      if (resJson.res) {
        const aryRes = [];
        for (const no in resJson.res) {
          const res = {...resJson.res[no]};
          // resオブジェクトにreNoキー追加
          res.resNo = no;
          aryRes.push(res);
        }
        aryRes.sort(x => x.rsc);

        // 取得したレス情報を手書き情報に格納する
        // 参照渡しをさせないためにスプレッド構文(中身を全部展開する)をつかって末尾に連結
        tegaki.resList = [...tegaki.resList.concat([...aryRes])];
        // 次回API呼び出し用のレスNO
        tegaki.resNo = (Number(aryRes[aryRes.length - 1].resNo) + 1).toString();

        // 読み上げ処理
        for (const res of aryRes) {
          // 削除されたレスは読み飛ばし
          if (res.del !== "") {
            continue;
          }
          if (tegakiStorage.readIDRes) {
            // IDが空じゃない場合 読み飛ばし
            // ID表示スレの場合基本的にIDは格納されていないがdelされてidが出てる場合格納されている
            if (res.id !== "") {
              continue;
            }
          }

          // コメントのコピーを作成
          let com = res.com;

          // ---------------------------
          // 読み上げ前のプラグインを実行
          // ---------------------------
          {
            const pResult = runPlugins({ point: "read", execName: "beforeExecute", from: "tegakiSaveCtrl.read", res: res });
            // ストップの場合、読み飛ばし（エラーの場合は続行するので無視）
            if (pResult.isStop) {
              continue;
            } else if (pResult.isError) {
              // エラーの場合、ログを出して続行（プラグインの処理がエラーで失敗しても読み上げは行う）
              console.error(pResult.message);
            } else if (pResult.resultValue != "") {
              // 値が帰ってきている場合、返ってきた値でコメントを上書きする
              com= pResult.resultValue;
            }
          }
          
          // レス読み上げを行うがONの場合のみ読み上げる
          if (tegakiStorage.readRes) {
            let readFlag = true;
            if (tegaki.initResIndex) {
              if (res.rsc <= tegaki.initResIndex) {
                readFlag = false;
              } else {
                // 初期読み上げフラグは使用したのでnullにする
                // スレ落ち後にrscが再度振られる可能性がある
                tegaki.initResIndex = null;
              }
            }

            // １行ずつ読み上げ
            if (readFlag) {
              for(const line of com.split("<br>")) {
                // タグを除去
                const text = line.replace(/<("[^"]*"|'[^']*'|[^'">])*>/g, '')
                  // 絵文字がHTML Entity形式（&amp;#000000;)の形式になっているので文字に復元する
                  .replace(/&#(.*?);/g, (m, p1) => String.fromCodePoint(`0${p1}`))
                  // レス文章に付ける特殊タグ
                  .replace(/(&gt;)+/, tegakiStorage.resTag);

                // 読み上げリストに追加
                speakText.push(text);
              }
            }
          }

          // ダイアログへレスを画面表示
          tegakiDialog.addResView({ ...res });

          // ---------------------------
          // 読み上げ後のプラグインを実行
          // 終端なのでエラーやストップでも処理を継続
          // ---------------------------
          {
            const pResult = runPlugins({ point: "read", execName: "afterExecute", from: "tegakiSaveCtrl.read", res: res });
            if (pResult.isError) {
              // エラーの場合、ログを出して続行
              console.error(pResult.message);
            }
          }
        }
        // ダイアログ内で最下部へスクロール
        tegakiDialog.scrollIntoView(aryRes[aryRes.length - 1].resNo);
      }
      // スレッド寿命表示の更新
      dieview();

      // スレッド最大の値が入っている場合
      if(resJson.maxres) {
        // （"maxres":"上限1000レスに達しました"）を読み上げに追加する。
        speakText.push(resJson.maxres);
        // レス取得を停止してボタンをグレイアウトにする
        thrLive.tegakiSave = false;
        document.getElementById("liveButton").style.backgroundColor = "gray";
        // 1000レスに達したのを画面に表示する
        const ds1 = document.getElementById("dispStatus1");
        ds1.innerHTML = `<strong>${resJson.maxres}</strong>`;
      }

      // 棒読みちゃんに読み上げ要求
      // バックグラウンドへ棒読みちゃん読み上げ処理要求
      if (speakText.length > 0) {
        bgSendMessage({ case: "speak", text: speakText, port: tegakiStorage.portNumber });
      }

      // スレ落ち判定
      if (new Date(resJson.dielong).getTime() < (resJson.nowtime * 1000)) {
        const text = "スレッドが落ちました。ライブを停止します。";
        // 手書き保存情報をスレ落ち状態に変更
        tegaki.isThreadLive = false;

        // ---------------------------
        // スレ落ち時のプラグインを実行
        // ---------------------------
        {
          const pResult = runPlugins({ point: "status404", execName: "execute", from: "tegakiSaveCtrl.read" });
          if (pResult.isStop || pResult.isError) {
            console.error(pResult.message);
          }
        }

        // タブのタイトルをスレ落ち状態にする
        document.title = "[スレ落ち]" + tegaki.threadTitle;
        document.getElementById("dispStatus1").innerHTML = "<strong>スレッドが落ちました</strong>";
        document.getElementById("dispStatus2").innerHTML = "";
        document.getElementById("dispStatus3").innerHTML = "";
    
        // スレッドが落ちてる場合、txtファイルの内容を変えたりする
        if (tegakiStorage.changeNo) {
          const message = {
            case: "thread",
            file: tegakiStorage.changeNoTxt
          };
          // 手書き制御にthread処理要求
          this.thread(message);
        }
    
        // スレ立てアラーム
        if (tegakiStorage.endAlarm) {
          // スレ立てアラームの実行時間を設定してsendEndAlarmを呼び出し
          tegakiDialog.processMessage("設定実行時間後にアラーム要求実行…");
          bgSendMessage({ case: "timeout", name: "sendEndAlarm", time: tegakiStorage.endAlarmTime * 60 * 1000 });
        }

        // バックグラウンドへ棒読みちゃん読み上げ処理要求
        bgSendMessage({ case: "speak", text: [text], port: tegakiStorage.portNumber });
      }
    }
    catch (e) {
      // 「操作が早すぎる」の応答があった場合、何もせず終わる(通常発生しないが、カタログ荒らしなどが出た際に出やすい)
      console.log("tegakiSaveCtrl read error: ", e, xmlHttpApi.responseText);
    }
    return;
  }

  // ファイルの保存
  save(message) {
    tegakiDialog.processMessage("手書きなどのファイル保存の実行中…");

    // ---------------------------
    // 保存前のプラグインを実行
    // ---------------------------
    let pResult = {};
    // 読み上げ後のプラグインを実行
    // 終端なのでエラーやストップでも処理を継続
    pResult = runPlugins({ point: "save", execName: "beforeExecute", from: "tegakiSaveCtrl.save", res: message.res });
    // ストップの場合、保存せずにスルーする
    if (pResult.isStop) {
      return;
    }
    // 画像ファイルの場合
    if (/\.(jpg|jpeg|png|gif|bmp)$/.test(message.file)){
      // 手書きファイルの保存
      let fileName = message.file;
      let img = new Image();
      img.src = message.url;
      img.onload = function () {
        var url = message.url;

        // デフォサイズ以外の判定
        if (message.custom && (img.width != 344 || img.height != 135)) {
          fileName = message.customName;
        }
        if (message.fixWidth) {
          var canvas = document.createElement("canvas");
          canvas.height = img.height;
          canvas.width = img.width;
          var ctx = canvas.getContext('2d');
          var scale = message.widthPx / img.width;
          canvas.height = canvas.height * scale;
          canvas.width = canvas.width * scale;
          ctx.scale(scale, scale);
          ctx.drawImage(img, 0, 0);
          url = canvas.toDataURL('image/png');

          var blob = dataURItoBlob(url);
          const downloadOptions = { url: URL.createObjectURL(blob), filename: fileName, conflictAction: "overwrite" };
          message.case = "download";
          message.downloadOptions = downloadOptions;
          bgSendMessage(message);
        } else {
          const downloadOptions = { url: url, filename: fileName, conflictAction: "overwrite" };
          message.case = "download";
          message.downloadOptions = downloadOptions;
          bgSendMessage(message);
        }
      }
    } else {
      // 画像ファイル以外の場合、そのままダウンロード
      const downloadOptions = { url: message.url, filename: message.file, conflictAction: "overwrite" };
      message.case = "download";
      message.downloadOptions = downloadOptions;
      bgSendMessage(message);
    }

    // ---------------------------
    // 保存後のプラグインを実行
    // 終端なのでエラーやストップでも処理を継続
    // ---------------------------
    pResult = runPlugins({ point: "save", execName: "afterExecute", from: "tegakiSaveCtrl.save", res: message.res });
    if (pResult.isError) {
      // エラーの場合、ログを出して続行
      console.error(pResult.message);
    }
  }

  // 手書きのクリア処理
  clear() {
    tegakiDialog.processMessage("１定時間後に手書きをクリアする処理実行中…");
    var canvas = document.createElement("canvas");
    let url = canvas.toDataURL('image/png');

    var blob = dataURItoBlob(url);
    const downloadOptions = { url: URL.createObjectURL(blob), filename: "tegaki.png", conflictAction: "overwrite" };
    let message = {};
    message.case = "download";
    message.downloadOptions = downloadOptions;
    bgSendMessage(message);
  }
}

// Blob型変換（手書き保存用処理）
function dataURItoBlob(dataURI) {
  var byteString = atob(dataURI.split(',')[1]);
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ab], { type: mimeString });
}

// スレッド寿命表示の更新
function dieview() {
  // スレ落ち時刻が空の場合、すぐ終了
  if (tegaki.threadDielong == "") {
    return;
  }

  const dieLong = new Date(tegaki.threadDielong);

  // スレ落ちまでの残り分数を算出
  const lestTime = dieLong.getTime() - tegaki.serverTime;
  const lestMinute = parseInt(lestTime / (60 * 1000), 10);
  const lestSecond = parseInt(lestTime % (60 * 1000) / 1000, 10);

  const ds1 = document.getElementById("dispStatus1");
  const ds2 = document.getElementById("dispStatus2");

  if (tegaki.isThreadOld) {
    ds1.innerHTML = "<strong>このスレは古いので、もうすぐ消えます</strong>";
  }

  const timeString = dieLong.toLocaleTimeString('ja-JP', { hour12: false, hour: 'numeric', minute: 'numeric' });
  ds2.textContent = `${timeString}頃消えます(あと${lestMinute}分${lestSecond}秒くらい)`;

}


//----------------------------------------------------
// ダイアログ処理
class TegakiDialog {
  constructor() {
    // ダイアログのzIndex（開始ボタンエリア:10、背景:11、タイトル:12、メインダイアログ:20、スライドメニュー:30、ボタン:40、削除用ダイアログ:50,51）

    // ふたクロの処理の影響外に要素を追加するため
    // 手書き保存ツール専用の領域をページの先頭に設置
    const tegakiDiv = document.createElement('div');
    tegakiDiv.id = 'tegaki_save';
    const firstChild = document.body.firstChild;        // 最初の子要素を取得
    document.body.insertBefore(tegakiDiv, firstChild);  // ページの先頭へ手書き保存ツール専用の要素を追加

    // ボタン部の要素を作成
    this.startButton = document.createElement('div');
    this.startButton.id = 'buttonContent';
    this.startButton.className = 'tedm';
    this.startButton.style.position = 'fixed';
    this.startButton.style.bottom = '20px';
    this.startButton.style.right = '80px';  // 右寄せにする
    this.startButton.style.left = 'auto';  // leftをautoにする
    this.startButton.style.width = '375px';
    this.startButton.style.height = '80px';
    this.startButton.style.padding = '0px';
    this.startButton.style.border = 'none';
    this.startButton.style.display = 'block';;
    this.startButton.style.zIndex = '10';   // 開始ボタンエリアはzIndex:10
    this.startButton.style.transition = 'right 0.3s ease-in-out, width 0.3s ease-in-out';

    // fieldset要素を生成
    const fieldset = document.createElement('fieldset');
    fieldset.className = 'tefieldset';
    // legend要素を生成
    const legend = document.createElement('legend');
    legend.textContent = `手書き保存ツール ver${tegaki.version}`;
    fieldset.appendChild(legend);
    if(location.href.startsWith("https://")) {
      // ボタンを追加
      const button1 = document.createElement('button');
      button1.id = 'tegakiButton';
      button1.className = 'tebtn';
      button1.textContent = '開始';
      button1.title = '手書き保存を開始します';
      const button2 = document.createElement('button');
      button2.id = 'tegakiMiddleButton';
      button2.className = 'tebtn';
      button2.textContent = '表示中の最終レス以降から開始';
      button2.title = '読み上げは表示中の最終レス以降から・手書きはすべて保存';
      const button3 = document.createElement('button');
      button3.id = 'tegakiDialogButton';
      button3.className = 'tebtn';
      button3.style.display = 'none';
      button3.textContent = '画面表示';
      // ボタンクリック時の動きを追加
      button1.addEventListener('click', () => {
        button1.style.display = 'none';
        button2.style.display = 'none';
        button3.style.display = 'block';
        saveStart("init");
      });
      button2.addEventListener('click', () => {
        button1.style.display = 'none';
        button2.style.display = 'none';
        button3.style.display = 'block';
        saveStart("middle");
      });
      button3.addEventListener('click', () => {
        // 手書き保存画面を表示
        this.show();
      });
      // ボタン追加
      fieldset.appendChild(button1);
      fieldset.appendChild(button2);
      fieldset.appendChild(button3);
    } else {
      // HTTPSではない場合遷移するボタンを置いて終了
      const button1 = document.createElement('button');
      button1.id = 'tegakiButton';
      button1.className = 'tebtn';
      button1.textContent = 'HTTPSに移動';
      button1.title = 'HTTPは対応していなのでHTTPSに移動します';
      button1.addEventListener('click', () => {
        location.href = location.href.replace(/^[^:]+/, "https");
      });
      fieldset.appendChild(button1);
    }
    // 開始ボタンエリアにボタンを含んだ枠を追加
    this.startButton.appendChild(fieldset);
    
    // スライドボタンを追加して、ボタンエリアを表示/非表示できるようにする
    const sliderButton = document.createElement('div');
    sliderButton.className = 'tedt';
    sliderButton.innerText = '>';
    sliderButton.title = '手書き保存ツールボタンエリアを表示/非表示';
    sliderButton.style.position = 'absolute';
    sliderButton.style.left = '-20px';
    sliderButton.style.top = '0px';
    sliderButton.style.width = '20px';
    sliderButton.style.height = '100%';
    sliderButton.style.backgroundColor = '#ea8';
    sliderButton.style.cursor = 'pointer';
    sliderButton.style.display = 'flex';
    sliderButton.style.justifyContent = 'center';
    sliderButton.style.alignItems = 'center';
    sliderButton.style.borderTopLeftRadius = '5px';
    sliderButton.style.borderBottomLeftRadius = '5px';

    // ボタンエリアに追加する
    this.startButton.appendChild(sliderButton);

    // つまみ要素にクリックイベントを追加
    let isShown = true;  // ダイアログが表示されているかどうか
    sliderButton.addEventListener('click', () => {
      if (sliderButton.innerText === '>') {
        this.startButton.style.right = '-375px';
        sliderButton.innerText = '<';
      } else {
        this.startButton.style.right = '80px';
        sliderButton.innerText = '>';
      }
    });

    // 開始ボタンエリアをbodyに追加
    document.getElementById('tegaki_save').appendChild(this.startButton);

    // ダイアログ範囲外
    this.bgWindow = document.createElement("div");
    this.bgWindow.id = "dialogBackground";
    this.bgWindow.style.width = "100%";
    this.bgWindow.style.height = "100%";
    this.bgWindow.style.position = "fixed";
    this.bgWindow.style.top = "0px";
    this.bgWindow.style.left = "0px";
    this.bgWindow.style.backgroundColor = "rgb(0, 0, 0)";
    this.bgWindow.style.opacity = "0.5";
    this.bgWindow.style.display = 'none';
    this.bgWindow.style.zIndex = "11";   // 背景はzIndex:10  

    // タイトル要素を作成
    this.title = document.createElement('div');
    this.title.className = 'tedt';
    this.title.style.position = 'fixed';
    this.title.style.top = '40px';
    this.title.style.right = '20px';  // 右寄せにする
    this.title.style.left = 'auto';  // leftをautoにする
    this.title.style.width = '70%';
    this.title.style.height = '20px';
    this.title.style.padding = '10px';
    this.title.style.display = 'none';
    this.title.style.zIndex = '12';   // タイトルはzIndex:11

    // タイトルコンテンツ
    const titleContent = document.createElement('div');
    const titleText = document.createElement('span');
    titleText.textContent = `手書き保存ツール ver${tegaki.version}`;
    titleText.style.marginLeft = "10px";
    titleText.style.fontSize = "small";
    titleText.style.position = 'absolute';
    titleText.style.top = '10px';
    titleText.style.left = '10px';
    titleContent.appendChild(titleText);
    // ボタン要素を作成
    const closeButton = document.createElement('button');
    closeButton.className = 'tedt';
    closeButton.textContent = '×';
    closeButton.title = "画面を閉じる";
    closeButton.style.fontSize = "small";
    closeButton.style.position = 'absolute';
    closeButton.style.top = '10px';
    closeButton.style.right = '10px';
    titleContent.appendChild(closeButton);

    this.title.appendChild(titleContent);

    // メインダイアログ要素を作成
    this.dialog = document.createElement('div');
    this.dialog.className = 'tedm';
    this.dialog.style.position = 'fixed';
    this.dialog.style.top = '80px';
    this.dialog.style.right = '20px';  // 右寄せにする
    this.dialog.style.left = 'auto';  // leftをautoにする
    this.dialog.style.width = '70%';
    this.dialog.style.height = '85%';
    this.dialog.style.padding = '10px';
    this.dialog.style.display = 'none';
    this.dialog.style.overflowY = 'scroll'; // スクロール可能にする
    this.dialog.style.overflowX = 'hidden';
    this.dialog.style.zIndex = '20';   // ダイアログ画面はzIndex:20

    // スレッド本文とスレッド画像を表示
    // div要素を作成
    const divElem = document.createElement("div");
    // img要素を作成
    const imgElem = document.createElement("img");
    // imgElem.id = "threthumb";
    imgElem.src = tegaki.threadImgUrl;
    imgElem.style.border = "none";
    imgElem.style.float = "left";
    imgElem.style.marginRight = "20px";
    imgElem.loading = "lazy";
    divElem.appendChild(imgElem);

    // スレッド本文の日時 レスNo hh:mm頃消えます を表示
    const cnwElem = document.createElement("span");
    cnwElem.classList.add("tecntd");
    cnwElem.textContent = document.querySelector('.thre').querySelector('.cnw').textContent;
    cnwElem.style.marginRight = "10px";
    divElem.appendChild(cnwElem);

    const cnoElem = document.createElement("span");
    cnoElem.classList.add("tecntd");
    cnoElem.textContent = document.querySelector('.thre').querySelector('.cno').textContent;;
    cnoElem.style.marginRight = "10px";
    divElem.appendChild(cnoElem);

    const cntdElem = document.createElement("span");
    cntdElem.classList.add("tecntd");
    cntdElem.textContent = document.querySelector('.thre').querySelector('.cntd').textContent;;
    cntdElem.style.marginRight = "10px";
    divElem.appendChild(cntdElem);

    const blockquoteElem = document.createElement("blockquote");
    blockquoteElem.innerHTML = tegaki.threadHonbun.replace(/(<a href=["'])\/bin\/jump\.php\?/g, "<a href=\"");;
    divElem.appendChild(blockquoteElem);

    // ダイアログにスレッド本文とスレッド画像を表示
    this.dialog.appendChild(divElem);


    // レスとスレッド状態の表示領域を設定
    // div要素を作成
    const thrDivElem = document.createElement("div");
    thrDivElem.classList.add("tethre");
    thrDivElem.style.width = "100%";
    thrDivElem.id = "tegakiThread";

    // hr要素を作成
    const hrElem1 = document.createElement("hr");

    // span要素を作成
    const spanElem10 = document.createElement("span");
    // spanElem10.style.marginLeft = "24px";
    spanElem10.style.fontSize = "100%";
    // spanElem10.style.left = "1.5em";
    spanElem10.style.color = "#cc1105";

    // span要素を作成
    const spanElem11 = document.createElement("div");
    spanElem11.id = "dispStatus1";
    spanElem10.appendChild(spanElem11);

    // span要素を作成
    const spanElem12 = document.createElement("div");
    spanElem12.id = "dispStatus2";
    spanElem12.textContent = "処理中だからちょっと待ってね";
    spanElem10.appendChild(spanElem12);

    // span要素を作成
    const spanElem13 = document.createElement("div");
    spanElem13.id = "dispStatus3";
    spanElem10.appendChild(spanElem13);
    
    // hr要素を作成
    const hrElem2 = document.createElement("hr");

    // 要素を追加 レスとスレッド状態の表示領域を設定
    this.dialog.appendChild(thrDivElem);
    this.dialog.appendChild(hrElem1);
    this.dialog.appendChild(spanElem10);
    this.dialog.appendChild(hrElem2);

    // イベントリスナーを設定してダイアログを閉じる
    closeButton.addEventListener('click', () => {
      this.bgWindow.style.display = 'none';
      this.title.style.display = 'none';
      this.dialog.style.display = 'none';
      this.startButton.style.display = 'block';
      document.body.style.overflow = ''; // 元画面のスクロールを有効にする
    });

    // 背景をbodyに追加
    document.getElementById('tegaki_save').appendChild(this.bgWindow);
    // タイトルをbodyに追加
    document.getElementById('tegaki_save').appendChild(this.title);
    // ダイアログをbodyに追加
    document.getElementById('tegaki_save').appendChild(this.dialog);

    // スライドメニュー
    // スライドメニューの要素を作成
    this.slideMenu = document.createElement('div');
    this.slideMenu.className = 'tedt';
    this.slideMenu.style.position = 'absolute';
    this.slideMenu.style.top = '0';
    this.slideMenu.style.right = '-300px';
    this.slideMenu.style.width = '300px';
    this.slideMenu.style.height = '100%';
    this.slideMenu.style.transition = 'right 0.3s ease-in-out, width 0.3s ease-in-out';
    this.slideMenu.style.zIndex = '30';   // スライドメニュー画面はzIndex:30

    // スライドメニューの中身を作成
    const slideMenuContent = document.createElement('div');
    slideMenuContent.id = 'slideMenuContent';
    slideMenuContent.className = 'tesmc';
    slideMenuContent.style.padding = '10px';
    slideMenuContent.innerHTML = '<p><strong>プラグインなどの拡張要素追加場所</strong></p>';
    // プロセスメッセージ用要素を追加
    const processMessageDisplay = document.createElement("div");
    processMessageDisplay.id = "processDisplay";
    processMessageDisplay.innerText = '■実行中のプロセスを…[表示する]';


    const processMessageContent = document.createElement("div");
    processMessageContent.id = "processMessage";
    processMessageContent.style.margin = '5px 5px 5px 5px';
    processMessageContent.style.fontSize = 'x-small';
    processMessageContent.style.display = 'none';

    processMessageDisplay.addEventListener('click', () => {
      const pd = document.getElementById("processDisplay");
      const pm = document.getElementById("processMessage");
      if (pm.style.display === 'none') {
        pm.style.display = "block";
        pd.innerText = '■実行中のプロセスを…[隠す]';
      } else {
        pm.style.display = "none";
        pd.innerText = '■実行中のプロセスを…[表示する]';
      }
    });

    slideMenuContent.appendChild(processMessageDisplay);
    slideMenuContent.appendChild(processMessageContent);
    this.slideMenu.appendChild(slideMenuContent);

    // スライドメニューのボタン要素を作成
    const slideMenuButton = document.createElement('button');
    slideMenuButton.className = 'tedt';
    slideMenuButton.innerText = '≡';
    slideMenuButton.title = "スライドメニューを表示/非表示";
    slideMenuButton.style.position = 'fixed';
    slideMenuButton.style.top = '100px';
    slideMenuButton.style.right = '50px';
    slideMenuButton.style.width = '50px';
    slideMenuButton.style.height = '50px';
    slideMenuButton.style.fontSize = '30px';
    slideMenuButton.style.cursor = 'pointer';
    slideMenuButton.style.boxShadow = '2px 2px 3px 0px #777';
    slideMenuButton.style.zIndex = '40';   // ボタンはzIndex:40
    slideMenuButton.addEventListener('click', () => {
      if (this.slideMenu.style.right === '0px') {
        this.slideMenu.style.right = '-300px';
      } else {
        this.slideMenu.style.right = '0px';
      }
    });

    // ダイアログスクロールにスライドメニュー位置を合わせる
    this.dialog.addEventListener('scroll', () => {
      const scrollTop = this.dialog.scrollTop - 1;
      this.slideMenu.style.top = scrollTop + 'px';
    });

    // ダイアログにスライドメニュー要素を追加
    this.dialog.appendChild(this.slideMenu);
    this.dialog.appendChild(slideMenuButton);


    // 手書き保存の実行と中断ボタン要素を作成
    const liveButton = document.createElement('div');
    liveButton.id = "liveButton";
    liveButton.title = "手書き保存の実行/中断";
    liveButton.className = 'tedt';
    liveButton.style.position = 'fixed';
    liveButton.style.bottom = '60px';
    liveButton.style.right = '120px';
    liveButton.style.width = '50px';
    liveButton.style.height = '50px';
    liveButton.style.backgroundImage = `url(${chrome.runtime.getURL('../icon/live_50.png')})`;
    liveButton.style.cursor = 'pointer';
    liveButton.style.margin = '2px';
    liveButton.style.boxShadow = '2px 2px 3px 0px #777';
    liveButton.style.zIndex = '40';   // ボタンはzIndex:40
    liveButton.addEventListener('click', () => {
      if (thrLive.tegakiSave) {
        thrLive.tegakiSave = false;
        liveButton.style.backgroundColor = "gray";
      } else {
        thrLive.tegakiSave = true;
        liveButton.style.backgroundColor = '#F0E0D6';
      }
    });

    // 最下部へのスクロールのON/OFFボタン要素を作成
    const scrollButton = document.createElement('div');
    scrollButton.title = "最下部へのスクロールのON/OFF";
    scrollButton.className = 'tedt';
    scrollButton.style.position = 'fixed';
    scrollButton.style.bottom = '60px';
    scrollButton.style.right = '50px';
    scrollButton.style.width = '50px';
    scrollButton.style.height = '50px';
    scrollButton.style.backgroundImage = `url(${chrome.runtime.getURL('../icon/scroll_50.png')})`;
    scrollButton.style.cursor = 'pointer';
    scrollButton.style.margin = '2px';
    scrollButton.style.boxShadow = '2px 2px 3px 0px #777';
    scrollButton.style.zIndex = '40';   // ボタンはzIndex:40
    scrollButton.addEventListener('click', () => {
      if (thrLive.autoScroll) {
        thrLive.autoScroll = false;
        scrollButton.style.backgroundColor = "gray";
      } else {
        thrLive.autoScroll = true;
        scrollButton.style.backgroundColor = '#F0E0D6';
      }
    });

    // ボタンを画面に追加
    this.dialog.appendChild(liveButton);
    this.dialog.appendChild(scrollButton);
    
  }
  // ダイアログの表示
  show() {
    this.bgWindow.style.display = 'block';
    this.title.style.display = 'block';
    this.dialog.style.display = 'block';
    this.startButton.style.display = 'none';
    document.body.style.overflow = 'hidden'; // 元画面のスクロールを無効にする

  }
  // レス追加
  addResView(res) {
    // 画面表示ID内領域取得
    const thr = document.getElementById("tegakiThread");
    // レスを画面表示
    thr.appendChild(this.createResTag(res));
  }
  // 最下部へスクロール
  scrollIntoView(resNo) {
    // 新着レスがあった場合
    if(tegaki.storedResNo !== resNo) {
      tegaki.storedResNo = resNo;

      // 画面表示ID内領域取得
      const thr = document.getElementById("tegakiThread");
      // 区切りの横線を挿入
      // thr.appendChild(document.createElement('hr'));
      const hr = document.querySelectorAll('.tehrtext');
      for (let n = 0; n < hr.length - 1; n++) {
        hr[n].remove();
      }
      const p = document.createElement('p');
      p.className = "tehrtext";
      p.textContent = '手書き保存ツールが新着レスを取得した';
      thr.appendChild(p);
      
      // 自動スクロールがONの時のみスクロール
      if (thrLive.autoScroll) {
        this.dialog.scrollTop = this.dialog.scrollHeight - this.dialog.clientHeight;
      }
    }
  }
  // プロセスメッセージ表示
  processMessage(text) {
    const view = document.getElementById("processMessage");
    const d = new Date();
    const t = `&nbsp${('0' + d.getHours()).slice(-2)}:${('0' + d.getMinutes()).slice(-2)}:${('0' + d.getSeconds()).slice(-2)}&nbsp`;

    const messageList = [
      tegaki.processList[1],
      tegaki.processList[2],
      tegaki.processList[3],
      tegaki.processList[4],
      `${t}${text}`
    ];
    const messageHTML = messageList.map(message => `${message}<br>`).join('');

    view.innerHTML = messageHTML;
    tegaki.processList = messageList;
  }

  // 画面制御 レスのタグを作成
  createResTag(data) {
    const self = this;  // self に this を代入
    const t = document.createElement('table');
    t.style.width = '100%';

    const tb = document.createElement('tbody');
    const tr = document.createElement('tr');
    const rts = document.createElement('td');
    rts.className = "terts";
    rts.innerHTML = "…";

    const rtd = document.createElement('td');
    rtd.className = "tertd";

    // ID
    let t11 = '';
    if (data.id !== '') {
      t11 = `   ${data.id}   `;
    }

    // 名前行
    let t12 = document.createElement('span');
    t12.className = "tecnw";

    const t0 = document.createElement('span');
    t0.innerHTML += data.rsc;
    t0.className = "tersc";
    const t3 = document.createElement('span');
    if (data.email != "") {
      t3.innerHTML = "email[" + data.email + "]";
      t3.className = "tecnm";
    }
    t12.innerHTML = data.now;
    // ID
    t12.appendChild(document.createTextNode(t11));

    // 発言No
    const t4a = document.createElement('span');
    t4a.className = "tecno";
    t4a.innerHTML = "No." + data.resNo;
    t4a.style.fontSize = "small";
    t4a.addEventListener('click', () => {
      // ダイアログを作成する
      self.createDelDialog(t4a);
    });

    // ボタン要素を作成
    const hideButton = document.createElement('button');
    hideButton.className = 'tedt';
    hideButton.textContent = '×';
    hideButton.title = "レスを非表示にする";
    hideButton.style.fontSize = "small";
    hideButton.style.border = 'none';
    hideButton.addEventListener('click', () => {
      if (confirm("このレスを非表示にしますか？")) {
        // ダイアログ内にある該当レスの要素を隠す
        const table = t4a.closest("table");
        table.style.display = "none";
      }
    });

    // 添付ファイル
    const b1 = document.createElement('br');
    const t5 = document.createTextNode(" \u00A0 \u00A0 ");
    const a3 = document.createElement('a');
    const t7 = document.createElement('span');
    const b2 = document.createElement('br');
    const a4 = document.createElement('a');
    const i1 = document.createElement('img');
    // 添付ファイル
    if (data.ext != '') {
      a3.style.fontSize = "small";
      a3.appendChild(document.createTextNode(data.tim + data.ext));
      a3.href = "https://img.2chan.net" + data.src;
      a3.target = "_blank";

      t7.style.fontSize = "small";

      a4.href = "https://img.2chan.net" + data.src;
      a4.target = "_blank";

      i1.src = "https://img.2chan.net" + data.thumb;
      i1.style.border = "none";
      i1.style.float = "left";
      i1.style.marginRight = "20px";
      i1.style.marginLeft = "20px";
      i1.width = data.w;
      i1.height = data.h;
      i1.alt = data.fsize + " B";
    }

    // コメント本文
    const bl = document.createElement('blockquote');
    if (data.w > 0) {
      bl.style.marginLeft = (data.w + 40) + "px";
    }

    //URLやファイルのリンクを置換
    let comment = data.com;
    //URLを置換
    comment = comment.replace(/(<a href=["'])\/bin\/jump\.php\?/g, "<a href=\"");
    // //あぷ系のファイルリンクを置換
    if (comment.match(/([^a-zA-Z0-9#/]|^)([a-zA-Z]{1,2})([0-9]{4,})(\.[_a-zA-Z0-9]+|)/ig)) {
      comment = comment.replace(/([^a-zA-Z0-9#/]|^)([a-zA-Z]{1,2})([0-9]{4,})(\.[_a-zA-Z0-9]+|)/ig, function (p, q, r, u, v) {
        let url = "";
        switch (r) {
          case "f":
            url = "https://dec.2chan.net/up/";
            break;
          case "fu":
            url = "https://dec.2chan.net/up2/";
            break;
          default:
            return `${q}${r}${u}${v}`;
        }
        p = `${r}${u}${v}`;
        // url = `${url}src/${r}${u}`;
        // url = `${url}src/${p}`;
        url = `${url}src/${r}${u}${v}`;
        return `${q}<a href="${url}" data-orig="${r}" target="_blank">${p}</a>`;

      });
    }

    bl.innerHTML += comment;

    rtd.appendChild(t0);
    //  rtd.appendChild(t1);
    //  rtd.appendChild(t2);
    rtd.appendChild(t3);
    rtd.appendChild(t12);
    rtd.appendChild(t4a);//No
    rtd.appendChild(hideButton);
    // rtd.appendChild(a20);

    // 添付ファイル
    if (data.ext != '') {
      rtd.appendChild(b1);
      rtd.appendChild(t5);
      rtd.appendChild(a3);
      // rtd.appendChild(t6);
      rtd.appendChild(t7);
      rtd.appendChild(b2);
      a4.appendChild(i1);
      rtd.appendChild(a4);
    }
    rtd.appendChild(bl);

    tr.appendChild(rts);
    tr.appendChild(rtd);
    tb.appendChild(tr);
    t.appendChild(tb);
    return t;
  }

  // 削除用ダイアログ表示
  createDelDialog(t4a) {
    var resNo = t4a.innerText.replace("No.", "");

    // ダイアログ範囲外
    const delbgWindow = document.createElement("div");
    delbgWindow.id = "delDialogBackground";
    delbgWindow.style.width = "100%";
    delbgWindow.style.height = "100%";
    delbgWindow.style.position = "fixed";
    delbgWindow.style.top = "0px";
    delbgWindow.style.left = "0px";
    delbgWindow.style.zIndex = "50";
    delbgWindow.addEventListener('click', () => {
      // 削除ダイアログ範囲外をクリックしたさい、ダイアログを削除
      var windowElement = document.getElementById("delDialog");
      var backgroundElement = document.getElementById("delDialogBackground");
      windowElement.parentNode.removeChild(windowElement);
      backgroundElement.parentNode.removeChild(backgroundElement);
    });
    document.getElementById('tegaki_save').appendChild(delbgWindow);


    // ダイアログ要素の作成
    var dtop = t4a.getBoundingClientRect().bottom + window.pageYOffset;
    var dleft = t4a.getBoundingClientRect().left + window.pageXOffset;

    const delDialog = document.createElement('div');
    delDialog.id = "delDialog"; // IDを付与する
    delDialog.className = 'tepdmm';
    delDialog.style.position = "absolute";
    delDialog.style.top = dtop + 'px';
    delDialog.style.left = dleft + 'px';
    delDialog.style.zIndex = '51';   // 

    var d2 = document.createElement('div'); // 削除依頼
    d2.innerHTML = "削除依頼(del)";
    d2.className = "pdms";
    // d2.addEventListener("click", function () { delsend(arguments[0], resNo, "110"); }, false);
    d2.addEventListener('click', () => {
      // del要求を実行
      tegakiDelsend(resNo);
      // ダイアログを閉じる
      delbgWindow.click();
    });

    // 記事削除を削除ボタンのみにする（「画像だけ削除」はoff、パスワードはCookieから取得して使う、ローカライズした削除処理を呼び出す）、削除後に該当レスの表示を消す
    var d3 = document.createElement('div'); // 削除依頼
    d3.innerHTML = "記事削除";
    d3.className = "pdms";
    d3.addEventListener('click', () => {
      // 記事削除を実行
      tegakiUsrDelSend(resNo, t4a);
      // ダイアログを閉じる
      delbgWindow.click();
    });

    // 要素を追加
    delDialog.appendChild(d2);
    delDialog.appendChild(d3);
    delDialog.setAttribute('data-no', resNo); //レス番号
    delDialog.setAttribute('data-res', tegaki.threadName);    //スレッド番号

    // クリックした場所にダイアログを表示する
    document.getElementById('tegaki_save').appendChild(delDialog);

  }
}

// クッキーから削除用情報取得
function getDelCookie(key) {//get cookie
  const cookie = ` ${document.cookie};`;
  const len = cookie.length;
  let start = cookie.indexOf(` ${key}=`);
  if (start === -1) return '';
  start += key.length + 2;
  let end = cookie.indexOf(';', start);
  if (end === -1) end = len;
  return decodeURIComponent(cookie.substring(start, end));
}
// 記事削除の送信
function tegakiUsrDelSend(resNo, t4a) {
  const data = {
    pwd: getDelCookie("pwdc"),   // クッキーからパスワード取得
    responsemode: "ajax",
    onlyimgdel: "",       // 画像だけは常にoff
    mode: "usrdel",
    [resNo]: "delete"
  };

  // 記事削除要求
  const xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "/b/futaba.php?guid=on");
  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState === 4) {
      const res = xmlhttp.responseText;
      if (res === "ok") {
        try {
          // ダイアログ内にある該当レスの要素を消す
          const table = t4a.closest("table");
          table.style.display = "none";
        } catch (e){
          console.error("tegakiUsrDelSend :",e);
        }
      } else {
        alert(res);
      }
    }
  };
  xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlhttp.send(EncodeHTMLForm(data));
}

// パラメータ用にエンコード
function EncodeHTMLForm( data ){//Convert data to HTML form format
  return Object.entries(data)
    .map(([name, value]) => `${encodeURIComponent(name)}=${encodeURIComponent(value)}`)
    .join('&');
}

// del要求の送信
function tegakiDelsend(resNo) {
  var data = { "mode": "post", "b": "b", "d": resNo, "reason": '110', "responsemode": "ajax" };
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "/del.php");
  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4) {
      var res = xmlhttp.responseText;
      //			console.log(res);
      if (res == "ok") {
        alert("登録しました");
      } else {
        alert(res);
      }
    }
  };
  xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlhttp.send(EncodeHTMLForm(data));
}

