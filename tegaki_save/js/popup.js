// popup.js
// Ver2.1.0

// グローバル変数
var setting = {};
var numericTenp;
var pluginsInit = ["all.execute.first","read.check.ng.public","read.check.ng.user"];

$(function(){
  chrome.storage.local.get("setting", function (value) {
    setting = value.setting;

    // 構造体のチェックと初期化処理
    if (!setting) {
      setting = {};
      setting.autoRun    = false; // 本文に以下の文字がある場合自動スタート
      setting.keyword    = "";    // 条件
      setting.autoLive   = false; // ふたクロの自動更新も開始
      setting.overwrite  = false; // 透過pngで上書きする
      setting.saveSpan   = 10;    // 表示時間(秒)
      setting.fixWidth   = false; // 横幅を固定する
      setting.widthPx    = 344;   // 横幅(px)
      setting.otherSave  = false; // 元ファイルを別フォルダに保存する
      setting.otherDir   = "$Thread";    // 保存場所
      setting.customSave = false; // デフォサイズ以外を別フォルダに保存する
      setting.customDir  = "";    // 保存場所
      setting.shioSave   = false; // 塩辛瓶のファイルを保存する
      setting.shioDir    = "";    // 保存場所
      setting.threadNo   = false; // スレッドNoのtxtを保存する
      setting.changeNo   = false; // スレが落ちたらtxtを変える
      setting.changeNoTxt   = ""; // 変更後txt
      setting.alarm      = false; // アラームを鳴らす
      setting.alarmTime  = 5;      // 実行時間
      setting.startType  = 0;     // 開始時に読み上げ
      setting.startText  = "手書き保存を開始します";    // 読み上げ文章
      setting.saveType   = 0;     // 保存時に読み上げ
      setting.saveText   = "手書きを保存しました";    // 読み上げ文章
      setting.alarmType   = 0      // アラーム方式(wavファイル再生 / 棒読みちゃん連携)
      setting.alarmText   = "もうすぐスレッドが落ちます" // 読み上げ文章

      setting.endAlarm     = false;                   // スレ立てアラーム
      setting.endAlarmTime = 5;                       // 実行時間
      setting.endAlarmText = "スレッドが立ってません" // 読み上げ文
      setting.endAlarmChk  = true;                   // スレ立てアラーム

      setting.portNumber = 50080; // ポート番号(デフォルトは50080)
      setting.autoDel    = false; // IDのレスを自動で削除する
      setting.readRes    = false; // レス読み上げを行う
      setting.resTag     = "";    // レス文章に付ける特殊タグ
      setting.readIDRes  = false; // IDのレスを読まない

      setting.nonDialog          = false; // ダイアログ画面を表示しない
      setting.plugins    = pluginsInit;

      chrome.storage.local.set({'setting': setting}, function () {});    
    } else {
      // 再初期化フラグ
      var initFlg = false;
      // undefinedが読み上げられてしまう対策
      if (setting.changeNoTxt === undefined){
        setting.changeNoTxt   = ""; // 変更後txt
        initFlg = true;
      }

      if (setting.startText === undefined){
        setting.startText  = "手書き保存を開始します";    // 読み上げ文章
        initFlg = true;
      }

      if (setting.saveText === undefined){
        setting.saveText   = "手書きを保存しました";    // 読み上げ文章
        initFlg = true;
      }

      if (setting.alarmText === undefined){
        setting.alarmText   = "もうすぐスレッドが落ちます" // 読み上げ文章
        initFlg = true;
      }

      if (setting.endAlarmText === undefined){
        setting.endAlarmText = "スレッドが立ってません" // 読み上げ文
        initFlg = true;
      }

      if (setting.resTag === undefined){
        setting.resTag     = "";    // レス文章に付ける特殊タグ
        initFlg = true;
      }

      if (setting.plugins === undefined){
        setting.plugins    = pluginsInit;  // 初期値設定
        initFlg = true;
      } else if (setting.plugins.length === 0){
        setting.plugins    = pluginsInit;  // 初期値設定
        initFlg = true;
      }

      // 再初期化した場合上書き
      if (initFlg) {
        chrome.storage.local.set({'setting': setting}, function(){});
      }
    }

    // 各インプットの初期値設定

    // 本文に以下の文字がある場合自動スタート
    if (setting.autoRun) {
      $('#autoRun').prop("checked",setting.autoRun);
    }

    // 条件
    if (setting.keyword) {
      $("#keyword").val(setting.keyword);
    }

    // ふたクロの自動更新も開始
    if (setting.autoLive) {
      $('#autoLive').prop("checked",setting.autoLive);
    }

    // 透過pngで上書きする
    if (setting.overwrite) {
      $('#overwrite').prop("checked",setting.overwrite);
    }

    // ダイアログ画面を表示しない
    if (setting.nonDialog) {
      $('#nonDialog').prop("checked",setting.nonDialog);
    }

    // 表示時間(秒)
    if (setting.saveSpan) {
      $("#saveSpan").val(setting.saveSpan);
    } else {
      // 存在しない場合10秒
      $("#saveSpan").val(10);
    }

    // 横幅を固定する
    if (setting.fixWidth) {
      $('#fixWidth').prop("checked",setting.fixWidth);
    }

    // 横幅(px)
    if (setting.widthPx) {
      $("#widthPx").val(setting.widthPx);
    } else {
      // 存在しない場合344px(手書きデフォルト)
      $("#widthPx").val(344);
    }

    // 保存時に読み上げ
    if (setting.saveMsg) {
      $('#saveMsg').prop("checked",setting.saveMsg);
    }

    // 読み上げ文章
    if (setting.saveText) {
      $("#saveText").val(setting.saveText);
    }

    // 元ファイルを別フォルダに保存する
    if (setting.otherSave) {
      $('#otherSave').prop("checked",setting.otherSave);
    }

    // 保存場所
    if (setting.otherDir) {
      $("#otherDir").val(setting.otherDir);
    }

    // デフォサイズ以外を別フォルダに保存する
    if (setting.customSave) {
      $('#customSave').prop("checked",setting.customSave);
    }

    // 保存場所
    if (setting.customDir) {
      $("#customDir").val(setting.customDir);
    }

    // 塩辛瓶のファイルを保存する
    if (setting.shioSave) {
      $('#shioSave').prop("checked",setting.shioSave);
    }

    // 保存場所
    if (setting.shioDir) {
      $("#shioDir").val(setting.shioDir);
    }

    // スレッドNoのtxtを保存する
    if (setting.threadNo) {
      $('#threadNo').prop("checked",setting.threadNo);
    }

    // スレが落ちたらtxtを変える
    if (setting.changeNo) {
      $('#changeNo').prop("checked",setting.changeNo);
    }

    // 変更後txt
    if (setting.changeNoTxt) {
      $("#changeNoTxt").val(setting.changeNoTxt);
    } else {
      // 存在しない場合
      $("#changeNoTxt").val("");
    }

    // 開始時
    if (setting.startType == 0) {
      $('#startOff').prop("checked",true);
    } else if (setting.startType == 1) {
      $('#startWav').prop("checked",true);
    } else {
      $('#startBou').prop("checked",true);
    }

    // 読み上げ文章
    if (setting.startText) {
      $("#startText").val(setting.startText);
    } else {
      // 存在しない場合
      $("#startText").val("手書き保存を開始します");
    }

    // 開始時
    if (setting.saveType == 0) {
      $('#saveOff').prop("checked",true);
    } else if (setting.saveType == 1) {
      $('#saveWav').prop("checked",true);
    } else {
      $('#saveBou').prop("checked",true);
    }

    // 読み上げ文章
    if (setting.saveText) {
      $("#saveText").val(setting.saveText);
    } else {
      // 存在しない場合
      $("#saveText").val("手書きを保存しました");
    }

    // 開始時
    if (setting.alarmType == 0) {
      $('#alarmOff').prop("checked",true);
    } else if (setting.alarmType == 1) {
      $('#alarmWav').prop("checked",true);
    } else {
      $('#alarmBou').prop("checked",true);
    }

    // 読み上げ文章
    if (setting.alarmText) {
      $("#alarmText").val(setting.alarmText);
    } else {
      // 存在しない場合
      $("#alarmText").val("もうすぐスレッドが落ちます");
    }

    // 実行時間
    if (setting.alarmTime) {
      $("#alarmTime").val(setting.alarmTime);
    } else {
      // 存在しない場合5分
      $("#alarmTime").val(5);
    }

    // スレ立てアラーム
    if (setting.endAlarm) {
      $('#endAlarm').prop("checked",setting.endAlarm);
    }

    // 読み上げ文章
    if (setting.endAlarmText) {
      $("#endAlarmText").val(setting.endAlarmText);
    } else {
      // 存在しない場合
      $("#endAlarmText").val( "スレッドが立ってません");
    }

    // 実行時間
    if (setting.endAlarmTime) {
      $("#endAlarmTime").val(setting.endAlarmTime);
    } else {
      // 存在しない場合5分
      $("#endAlarmTime").val(5);
    }

    // ポート番号
    if (setting.portNumber) {
      $("#portNumber").val(setting.portNumber);
    } else {
      // デフォルトは50080
      $("#portNumber").val(50080);
    }

    // IDのレスを自動で削除する
    if (setting.autoDel) {
      $('#autoDel').prop("checked",setting.autoDel);
    }

    // レス読み上げを行う
    if (setting.readRes) {
      $('#readRes').prop("checked",setting.readRes);
    }

    // レス文章に付ける特殊タグ
    if (setting.resTag) {
      $("#resTag").val(setting.resTag);
    } else if (setting.resTag == null || setting.resTag == "undefined") {
      // 存在しない場合
      $("#resTag").val("");
    } else {
      // 存在しない場合
      $("#resTag").val("");
    }

    // IDのレスを読まない
    if (setting.readIDRes) {
      $('#readIDRes').prop("checked",setting.readIDRes);
    }

    // プラグインの読み込み状態
    if (setting.plugins) {
      $('#plugins').prop("title",setting.plugins.join("\n"));
    }

    $("#widthPx").prop("disabled", !setting.fixWidth);
    $("#changeNoTxt").prop("disabled", !setting.changeNo);

  });
});

////////////////////////////////////////////////////////////////////////////////////////////////////
// 各インプットの動作
// 本文に以下の文字がある場合自動スタート
$(document).on("change", "#autoRun", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.autoRun = $("#autoRun").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});
// 条件
$(document).on("change", "#keyword", function(){
  setting.keyword = $("#keyword").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});
// ふたクロの自動更新も開始
$(document).on("change", "#autoLive", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.autoLive = $("#autoLive").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 透過pngで上書きする
$(document).on("change", "#overwrite", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.overwrite = $("#overwrite").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// ダイアログ画面を表示しない
$(document).on("change", "#nonDialog", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.nonDialog = $("#nonDialog").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 表示時間(秒)
$(document).on("focus", "#saveSpan", function(){
  numericTenp = this.value;
});
$(document).on("change", "#saveSpan", function(){
  if (!$.isNumeric(this.value)) {
    this.value = numericTenp;
  } else if(this.value < 5)  {
    this.value = 5;
  }
  if (numericTenp != this.value) {
    setting.saveSpan = $("#saveSpan").val();
    chrome.storage.local.set({'setting': setting}, function(){});    
  }
});

// 横幅を固定する
$(document).on("change", "#fixWidth", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.fixWidth = $("#fixWidth").prop("checked");
    $("#widthPx").prop("disabled", !setting.fixWidth);
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 横幅(px)
$(document).on("focus", "#widthPx", function(){
  numericTenp = this.value;
});
$(document).on("change", "#widthPx", function(){
  if (!$.isNumeric(this.value)) {
    this.value = numericTenp;
  } else if(this.value < 1)  {
    this.value = 1;
  }
  if (numericTenp != this.value) {
    setting.widthPx = $("#widthPx").val();
    chrome.storage.local.set({'setting': setting}, function(){});
  }
});

// 元ファイルを別フォルダに保存する
$(document).on("change", "#otherSave", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.otherSave = $("#otherSave").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 保存場所
$(document).on("change", "#otherDir", function(){
  setting.otherDir = $("#otherDir").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// デフォサイズ以外を別フォルダに保存する
$(document).on("change", "#customSave", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.customSave = $("#customSave").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 保存場所
$(document).on("change", "#customDir", function(){
  setting.customDir = $("#customDir").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// 塩辛瓶のファイルを保存する
$(document).on("change", "#shioSave", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.shioSave = $("#shioSave").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 保存場所
$(document).on("change", "#shioDir", function(){
  setting.shioDir = $("#shioDir").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// スレッドNoのtxtを保存する
$(document).on("change", "#threadNo", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.threadNo = $("#threadNo").prop("checked");
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// スレが落ちたらtxtを変える
$(document).on("change", "#changeNo", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.changeNo = $("#changeNo").prop("checked");
    $("#changeNoTxt").prop("disabled", !setting.changeNo);
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});

// 変更後txt
$(document).on("change", "#changeNoTxt", function(){
  setting.changeNoTxt = $("#changeNoTxt").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// 開始時
$(document).on("change", "input[name=startType]", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.startType = $("input[name=startType]:checked").val();
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});
// 読み上げ文章
$(document).on("change", "#startText", function(){
  setting.startText = $("#startText").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// 保存時
$(document).on("change", "input[name=saveType]", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.saveType = $("input[name=saveType]:checked").val();
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});
// 読み上げ文章
$(document).on("change", "#saveText", function(){
  setting.saveText = $("#saveText").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// アラーム
$(document).on("change", "input[name=alarmType]", function(){
  chrome.storage.local.get("setting", function (value) {
    setting.alarmType = $("input[name=alarmType]:checked").val();
    chrome.storage.local.set({'setting': setting}, function(){});
  });
});
// 読み上げ文章
$(document).on("change", "#alarmText", function(){
  setting.alarmText = $("#alarmText").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// 実行時間
$(document).on("focus", "#alarmTime", function(){
  numericTenp = this.value;
});
$(document).on("change", "#alarmTime", function(){
  if (!$.isNumeric(this.value)) {
    this.value = numericTenp;
  } else if(this.value < 1)  {
    this.value = 1;
  }
  if (numericTenp != this.value) {
    setting.alarmTime = $("#alarmTime").val();
    chrome.storage.local.set({'setting': setting}, function(){});    
  }
});

// スレ立てアラーム
$(document).on("change", "#endAlarm", function(){
  setting.endAlarm = $("#endAlarm").prop("checked");
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// 読み上げ文章
$(document).on("change", "#endAlarmText", function(){
  setting.endAlarmText = $("#endAlarmText").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// 実行時間
$(document).on("focus", "#endAlarmTime", function(){
  numericTenp = this.value;
});
$(document).on("change", "#endAlarmTime", function(){
  if (!$.isNumeric(this.value)) {
    this.value = numericTenp;
  } else if(this.value < 1)  {
    this.value = 1;
  }
  if (numericTenp != this.value) {
    setting.endAlarmTime = $("#endAlarmTime").val();
    chrome.storage.local.set({'setting': setting}, function(){});    
  }
});

// ポート番号
$(document).on("focus", "#portNumber", function(){
  numericTenp = this.value;
});
$(document).on("change", "#portNumber", function(){
  if (!$.isNumeric(this.value)) {
    this.value = numericTenp;
  }
  if (numericTenp != this.value) {
    setting.portNumber = $("#portNumber").val();
    chrome.storage.local.set({'setting': setting}, function(){});    
  }
});

// IDのレスを自動で削除する
$(document).on("change", "#autoDel", function(){
  setting.autoDel = $("#autoDel").prop("checked");
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// レス読み上げを行う
$(document).on("change", "#readRes", function(){
  setting.readRes = $("#readRes").prop("checked");
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// レス文章に付ける特殊タグ
$(document).on("change", "#resTag", function(){
  setting.resTag = $("#resTag").val();
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// IDのレスを読まない
$(document).on("change", "#readIDRes", function(){
  setting.readIDRes = $("#readIDRes").prop("checked");
  chrome.storage.local.set({'setting': setting}, function(){});    
});

// プラグイン再読み込みクリック時
$(document).on("click", "#pluginReloadIcon", function(){
  // アイコンを回す
  pluginReloadIcon.style.transition = 'transform 1s';
  pluginReloadIcon.style.transform = 'rotate(720deg)';

  setTimeout(() => {
    pluginReloadIcon.style.transition = '';
    pluginReloadIcon.style.transform = '';
  }, 1000);

  // pluginsフォルダ内のファイルリスト取得
  chrome.runtime.getPackageDirectoryEntry(function(root) {
    root.getDirectory('plugins', {}, function(folder) {
      setting.plugins = [];
      folder.createReader().readEntries(function(entries) {
        entries.forEach(function(entry) {
          // フォルダ名を設定
          if (entry.isDirectory) {
            setting.plugins.push(entry.name);
          }
        });
        // settingに再設定
        chrome.storage.local.set({'setting': setting}, function(){});
        // マウスオーバーで表示する内容を変更
        $('#plugins').prop("title",setting.plugins.join("\n"));
      });
    });
  });
});

