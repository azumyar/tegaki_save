// background.js
// Ver2.1.0

let alarmNames = [];

// alarmを解除する
function clearAlarmsByTabId(tabId) {
  const indexes = [];
  for (let i = alarmNames.length - 1; i >= 0; i--) {
    const name = alarmNames[i];
    if (name.startsWith(tabId.toString())) {
      chrome.alarms.clear(name);
      indexes.push(i);
    }
  }
  indexes.forEach((index) => {
    alarmNames.splice(index, 1);
  });
}

// 送り先があるかどうか確認しつつ対象のタブにchrome.tabs.sendMessageで値を渡す
function sendMessageToContentScript(tabId, message) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.get(tabId, function(tab) {
      if (!chrome.runtime.lastError && tab) {
        chrome.tabs.sendMessage(tabId, message);
      }
    });
  });
  return true;
}

// 画面ロード時にタブIDを返す
// (chrome.tabs.onUpdated.addListenerはふたクロの書き込みフォームの子フレーム更新イベントの判定ができないのでこっちを使う)
chrome.webNavigation.onCompleted.addListener((details) => {
  // メインフレームの更新イベントを取り扱う
  if (details.url.startsWith("https://img.2chan.net/b/res/") || details.url.startsWith("http://img.2chan.net/b/res/")) {
    // 子フレームの更新イベントを無視する（ふたクロの書き込みフォーム用対応）
    if (details.frameId !== 0) {
      return;
    }
    // ロード時（再読み込み時）に該当タブで実行予定だったalarmを解除
    clearAlarmsByTabId(details.tabId);
    // ロード完了後、タブIDを返す
    sendMessageToContentScript(details.tabId, { case: "updateTab", tabId: details.tabId });
  }
}, {url: [{urlPrefix: "http://"}, {urlPrefix: "https://"}]});


// タブが閉じられた場合に該当する alarm を解除する
chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
  clearAlarmsByTabId(tabId);
});

// 画面からの要求リスナー メイン処理
chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.case === 'download') {
    // ファイルダウンロード
    chrome.downloads.download(message.downloadOptions, function(downloadId) {
      chrome.downloads.onChanged.addListener(function(downloadDelta) {
        if (downloadDelta.id === downloadId && downloadDelta.state && downloadDelta.state.current === "interrupted") {
          console.error("ダウンロードが中断されました");
          // 異常のあった場合、異常発生をsendMessageで返す
          sendMessageToContentScript(sender.tab.id, {error:true, case:message.case, filename: message.downloadOptions.filename});
        }
      });
      if (chrome.runtime.lastError) {
        // エラー処理
        console.error("download error:", chrome.runtime.lastError);
        // 異常のあった場合、異常発生をsendMessageで返す
        sendMessageToContentScript(sender.tab.id, {error:true, case:message.case, filename: message.downloadOptions.filename});
      } else {
        console.log(`Download ok started with ID:${downloadId} fileName:${message.downloadOptions.filename}`);
      }
    });
  } else if (message.case === 'speak') {
    // 棒読みちゃん連携
    speakText(message, sender, sendResponse);
  } else if (message.case === 'timeout') {
    // コンテンツスクリプト側ではオンクルージョン判定のためにsettimeoutなどが動かないことがあるので
    // setTimeout処理をここでやる
    let name = sender.tab.id.toString() + "_" + message.name;
    chrome.alarms.create(name, { when: Date.now() + message.time });
    alarmNames.push(name);
  }
  sendResponse({error:false, case:message.case});
  return true;
});

// alarmリスナー
chrome.alarms.onAlarm.addListener((alarm) => {
  let [tabId, name] = alarm.name.split("_");
  sendMessageToContentScript(parseInt(tabId), { case: "timeout", name: name });
});

// 棒読みちゃん連携
function speakText(message, sender, sendResponse) {
  const port = encodeURIComponent(message.port);

  let i = 0;

  // 連続して実行すると読み上げ順がおかしくなるので少し間を開けて実行する
  const interval = setInterval(() => {
    if (i >= message.text.length) {
      clearInterval(interval);
      sendResponse({ error: false, case: message.case });
      return;
    }
    
    const url = `http://localhost:${port}/talk?text=${encodeURIComponent(message.text[i])}`;
    // awaitしてsendResponseで返すと返した値がundefinedになるのでsendMessageで結果を返す
    fetch(url, {
      // Content-Type を指定する
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    }).then(response => {
      if (!response.ok) {
        console.error("speakText not ok:", response);
        // 異常のあった場合、異常発生をsendMessageで返す
        sendMessageToContentScript(sender.tab.id, {error:true, case:message.case, text: message.text});
      }
    }).catch(error => {
      console.error("speakText error:", error, decodeURIComponent(url));
      // 異常のあった場合、異常発生をsendMessageで返す
      sendMessageToContentScript(sender.tab.id, {error:true, case:message.case, text: message.text});
    });

    i++;
  }, 250);
}

